import React from "react";
import ReactDOM from "react-dom";
import LernaApp from "./lerna/LernaApp";
import * as serviceWorker from "./lerna/serviceWorker";

ReactDOM.render(
	<LernaApp />,
	document.getElementById("root")
);

serviceWorker.unregister();