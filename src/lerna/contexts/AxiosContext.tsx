import { AxiosStatic } from "axios"
import React, { FC, createContext, useState, useContext } from "react"
import axios from "../shared/axios"

export const AxiosContext = createContext<AxiosStatic>(null)

export const AxiosProvider: FC = (props) => {
	const [state] = useState<AxiosStatic>(() => axios())
	return (
		<AxiosContext.Provider value={state}>
			{props.children}
		</AxiosContext.Provider>
	)
}

export const useAxios = () => {
	return useContext<AxiosStatic>(AxiosContext)
}