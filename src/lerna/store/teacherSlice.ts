import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type Teacher = {
  teacher_id: string;
  teacher_name: string;
  teacher_surname: string;
  teacher_national_id: string;
  teacher_phone_number: string;
  teacher_password: string;
};

interface TeacherState {
  current: Teacher | null;
}

const initialState: TeacherState = {
  current: {
    teacher_id: "2064621520",
    teacher_name: "شهاب",
    teacher_surname: "یوسفی",
    teacher_national_id: "2064621520",
    teacher_phone_number: "09216873562",
    teacher_password: "12121212",
  },
};

export const teacherSlice = createSlice({
  name: "teacher",
  initialState,
  reducers: {
    set: (state, action: PayloadAction<Teacher | null>) => {
      state.current = action.payload;
    },
  },
});

export const { set } = teacherSlice.actions;

export default teacherSlice.reducer;
