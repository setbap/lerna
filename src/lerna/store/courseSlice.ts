import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type Course = {
  id: string;
  name: string;
};

interface courseState {
  courses: Course[];
}

const initialState: courseState = {
  courses: [
    {
      id: "‫ریاضی‬",
      name: "‫ریاضی‬",
    },
    {
      id: "‫فارسی‬",
      name: "‫فارسی‬",
    },
    {
      id: "هدیه آسمانی‬",
      name: "هدیه آسمانی",
    },
    {
      id: "‫علوم‬",
      name: "‫علوم‬",
    },
    {
      id: "‫اجتماعی‬",
      name: "‫اجتماعی‬",
    },
    {
      id: "کار و فناوری‬",
      name: "کار و فناوری‬",
    },
    {
      id: "‫هنر‬",
      name: "‫هنر‬",
    },
    {
      id: "زبان انگلیسی",
      name: "زبان انگلیسی",
    },
    {
      id: "سایر دروس",
      name: "سایر دروس",
    },
  ],
};

export const courseSlice = createSlice({
  name: "course",
  initialState,
  reducers: {
    set: (state, action: PayloadAction<Course[]>) => {
      state.courses = action.payload;
    },
  },
});

export const { set } = courseSlice.actions;

export default courseSlice.reducer;
