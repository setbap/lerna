import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import classReducer from "./classSlice";
import courseReducer from "./courseSlice";
import levelReducer from "./levelSlice";
import teacherReducer from "./teacherSlice";
import studentReducer from "./studentSlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    course: courseReducer,
    level: levelReducer,
    class: classReducer,
    teacher: teacherReducer,
    student: studentReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
