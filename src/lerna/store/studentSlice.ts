import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export type Student = {
  student_id: string;
  student_name: string;
  student_surname: string;
  student_national_id?: string;
  YearofBirth?: string;
  student_phone_number?: string;
  student_fatherName?: string;
  motherPhoneNumber?: string;
  fatherPhoneNumber?: string;
  student_password?: string;
  level?: string;
  class_name?: string;
};

interface StudentState {
  current: Student | null;
}

const initialState: StudentState = {
  current: {
    student_id: "2051959196",
    student_name: "شهاب",
    student_surname: "یوسفی",
    student_national_id: "2051959196",
    YearofBirth: "1377/04/14",
    student_phone_number: "24343",
    student_fatherName: "edwedsd",
    motherPhoneNumber: "23242",
    fatherPhoneNumber: "1221322",
    student_password: "12121212",
    level: "0",
    class_name: "10",
  },
};

export const studentSlice = createSlice({
  name: "student",
  initialState,
  reducers: {
    set: (state, action: PayloadAction<Student | null>) => {
      state.current = action.payload;
    },
  },
});

export const { set } = studentSlice.actions;

export default studentSlice.reducer;
