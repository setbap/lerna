import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type Level = {
  id: string;
  name: string;
};

interface levelState {
  levels: Level[];
}

const initialState: levelState = {
  levels: [
    {
      id: "0",
      name: "‫آمادگی‬",
    },
    {
      id: "1",
      name: "‫اول‬",
    },
    {
      id: "2",
      name: "‫دوم‬",
    },
    {
      id: "3",
      name: "‫سوم‬",
    },
    {
      id: "4",
      name: "‫چهارم‬",
    },
    {
      id: "5",
      name: "‫پنجم‬",
    },
    {
      id: "6",
      name: "‫ششم‬",
    },
  ],
};

export const levelSlice = createSlice({
  name: "level",
  initialState,
  reducers: {
    set: (state, action: PayloadAction<Level[]>) => {
      state.levels = action.payload;
    },
  },
});

export const { set } = levelSlice.actions;

export default levelSlice.reducer;
