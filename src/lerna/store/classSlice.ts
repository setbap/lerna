import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type Class = {
  id: string;
  name: string;
};

interface ClassState {
  classes: Class[];
}

const initialState: ClassState = {
  classes: [
    {
      id: "10",
      name: "‫الف‬",
    },
    {
      id: "11",
      name: "‫ب‬",
    },
    {
      id: "12",
      name: "‫ج‬",
    },
  ],
};

export const classSlice = createSlice({
  name: "class",
  initialState,
  reducers: {
    set: (state, action: PayloadAction<Class[]>) => {
      state.classes = action.payload;
    },
  },
});

export const { set } = classSlice.actions;

export default classSlice.reducer;
