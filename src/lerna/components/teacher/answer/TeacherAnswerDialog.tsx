import { Button, Dialog, IconButton, TextField, Toolbar } from "@material-ui/core";
import React, { FC, useEffect, useState } from "react";
import MyDialog from "../../utils/MyDialog";
import CommentIcon from "@material-ui/icons/Comment";
import { useAxios } from "../../../contexts/AxiosContext";
import { useSnackbar } from "notistack";
import { TaskAnswer } from "../../../pages/teacher/task";
import { toastResponseErrors } from "../../../shared/axios";
import CloseIcon from '@material-ui/icons/Close';
import { TransitionProps } from '@material-ui/core/transitions';
import Slide from '@material-ui/core/Slide';
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";

interface TeacherAnswerDialogProps {
	open: boolean;
	onClose: () => void;
	answer?: TaskAnswer
	onDone?: (answer: TaskAnswer | null) => void
}

const Transition = React.forwardRef(function Transition(
	props: TransitionProps & { children?: React.ReactElement },
	ref: React.Ref<unknown>,
) {
	return <Slide direction="up" ref={ref} {...props} />;
});

const TeacherAnswerDialog: FC<TeacherAnswerDialogProps> = ({
	open,
	answer,
	onClose,
	onDone
}) => {
	const [comment, setComment] = useState("")
	const [loading, setLoading] = useState(false)
	const [openImage, setOpenImage] = useState(false)

	const { enqueueSnackbar } = useSnackbar();
	const axios = useAxios();

	useEffect(() => {
		setComment(answer?.homeworkSolved_commentsTeacher || "");
	}, [open, answer?.homeworkSolved_commentsTeacher]);

	const validValue = (v: any) => {
		return v && v !== ""
	}

	const validUrl = (url?: string) => {
		return url && (url.toLocaleLowerCase().startsWith("http://") || url.toLocaleLowerCase().startsWith("https://"))
	}

	const save = async () => {
		if (!validValue(comment)) {
			enqueueSnackbar("لطفآ تمام فیلد ها را پر کنید", { variant: "error" })
			return
		}

		if (loading) return
		setLoading(true)

		let a = {
			homeworkSolved_id: answer?.homeworkSolved_id,
			homeworkSolved_homework_id: answer?.homeworkSolved_homework_id || "",
			homeworkSolved_student_id: answer?.homeworkSolved_student_id || "",
			homeworkSolved_commentsTeacher: comment || "",
			homeworkSolved_commentsStudent: answer?.homeworkSolved_commentsStudent || "",
			homeworkSolved_file: answer?.homeworkSolved_file || "",
			homeworkSolved_build_date: answer?.homeworkSolved_build_date || "",
			homeworkSolved_update_date: answer?.homeworkSolved_update_date || "",
		} as TaskAnswer

		try {
			const r = await axios.post("", {
				query: `   
		  mutation {
			updateHomeWorkInfoForStudent(
				homeWork_input_old: {
					homeworkSolved_id: "${a.homeworkSolved_id}"
				},
				homeWork_input_new: {
					homeworkSolved_id: "${a.homeworkSolved_id}"
					homeworkSolved_homework_id: "${a.homeworkSolved_homework_id}"
					homeworkSolved_student_id: "${a.homeworkSolved_student_id}"
					homeworkSolved_commentsTeacher: "${a.homeworkSolved_commentsTeacher}"
					homeworkSolved_commentsStudent: "${a.homeworkSolved_commentsStudent}"
					homeworkSolved_file: "${a.homeworkSolved_file}"
					homeworkSolved_build_date: "${a.homeworkSolved_build_date}"
					homeworkSolved_update_date: "${a.homeworkSolved_update_date}"
				}) {
					homeworkSolved_id
					homeworkSolved_homework_id
					homeworkSolved_student_id
					homeworkSolved_commentsTeacher
					homeworkSolved_commentsStudent
					homeworkSolved_file
					homeworkSolved_build_date
					homeworkSolved_update_date
				}
			}        
			`
			})

			const t = r.data.data.updateHomeWorkInfoForStudent

			if (t == null) {
				toastResponseErrors(r, enqueueSnackbar)
			} else {
				enqueueSnackbar("جواب بررسی شد", { variant: "success" })
				console.log(a)
				onDone && onDone(a)
				onClose && onClose()
			}

		} catch (e) {
			enqueueSnackbar(e.message, { variant: "error" })
			a = null
		}

		setLoading(false)
	};

	return (
		<MyDialog open={open} title="بررسی جواب" onClose={onClose} loading={loading}>
			<div className="p-4 flex flex-col">
				<div className="mt-0 lg:mt-4 mb-4 relative w-full max-w-md bg-natural-light rounded-2xl border border-natural-light overflow-hidden self-center">
					<div className="pt-9/16 w-full">
						<img
							className="absolute left-0 bg-white right-0 top-0 bottom-0 w-full h-full object-cover cursor-pointer"
							src={validUrl(answer?.homeworkSolved_file) ? answer?.homeworkSolved_file : "/lerna/img/placeholder.png"}
							alt=""
							onClick={() => setOpenImage(true)}
						/>
					</div>
				</div>
				<div className="mt-4 mx-0 lg:mx-4">
					<div className="flex flex-row items-center">
						<CommentIcon style={{ fontSize: 28, color: "#FF9800" }} />
						<span className="text-natural-dark font-bold text-lg ms-3">
							نظر دانش‌آموز
			</span>
					</div>
					<div className="flex flex-col mt-1">
						<div className="h-8 relative overflow-hidden z-10">
							<div className="absolute top-0 mt-4 start-0 ms-12 w-16 h-16 bg-natural-light transform rotate-45 " />
						</div>
						<div
							className="bg-natural-light p-4 rounded-t-2xl"
							style={{ minHeight: "140px" }}
						>
							<span className="text-natural-dark">{answer?.homeworkSolved_commentsStudent}</span>
						</div>
					</div>
				</div>
				<div className="mt-8 mx-0 lg:mx-4">
					<div className="flex flex-row items-center">
						<CommentIcon style={{ fontSize: 28, color: "#4CAF50" }} />
						<span className="text-natural-dark font-bold text-lg ms-3">
							نظر معلم
			</span>
					</div>
					<div className="flex flex-col mt-1">
						<div className="h-8 relative overflow-hidden z-10">
							<div className="absolute top-0 mt-4 start-0 ms-12 w-16 h-16 bg-natural-light transform rotate-45 " />
						</div>
						<TextField
							multiline
							variant="filled"
							label="نظر معلم"
							classes={{
								root: "",
							}}
							InputProps={{
								classes: {
									root: "rounded-t-2xl shadow-lg",
								},
							}}
							inputProps={{
								style: {
									minHeight: "103px",
								},
							}}
							value={comment}
							onChange={(e) => setComment(e.target.value || "")}
						/>
					</div>
				</div>
				<div className="flex mx-2 lg:mx-4 mb-1 lg:mb-2 mt-16 justify-between">
					<div className="flex-grow" />
					<Button
						onClick={save}
						variant="contained"
						style={{ borderRadius: "9999px", outline: "none" }}
						color="primary"
					>
						<span className="px-2 font-bold text-gray-800">ذخیره</span>
					</Button>
				</div>
			</div>
			<Dialog fullScreen open={openImage} onClose={() => setOpenImage(false)} TransitionComponent={Transition}>
				<div className="h-screen">
					<Toolbar className="absolute top-0 left-0 right-0 z-50">
						<div className="flex-grow" />
						<IconButton edge="end" color="inherit" onClick={() => setOpenImage(false)} aria-label="close">
							<CloseIcon />
						</IconButton>
					</Toolbar>
					<div className="pin-zoom relative w-screen h-screen overflow-hidden flex items-center justify-center">
						<TransformWrapper>
							<TransformComponent>
								<img src={validUrl(answer?.homeworkSolved_file) ? answer?.homeworkSolved_file : "/lerna/img/placeholder.png"} alt="test" />
							</TransformComponent>
						</TransformWrapper>
					</div>
				</div>

			</Dialog>
		</MyDialog>
	);
};

export default TeacherAnswerDialog;
