import React, { FC } from "react";
import FileList from "../../utils/FIleList";
import FileDrop from "../../utils/FileDrop";

interface CreateTaskImageFormProps {
	className?: string
	files?: any[]
	onFilesChange?: (files: any[]) => void
	setLoading?: (loading: boolean) => void
}

const CreateTaskImageForm: FC<CreateTaskImageFormProps> = ({ className, files, onFilesChange, setLoading }) => {

	const uploadFile = async (file: string | null) => {
		setLoading && setLoading(false)
		console.log("uploading file...", file || "no file")
		file && onFilesChange && file && onFilesChange([...files, file])
	};

	return (
		<div className={"flex flex-col " + (className || "")}>
			<FileDrop
				className="mx-0 lg:mx-4 my-4"
				onStart={() => (setLoading && setLoading(true))}
				onChange={uploadFile}
			/>
			<FileList
				className="mx-0 lg:mx-4 my-0 lg:my-4"
				files={files}
				onDownload={file => console.log(file)}
				onRemove={file => console.log(file)}
				removable
				size="large" />
		</div>
	)
}

export default CreateTaskImageForm
