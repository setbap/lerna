import React, { FC } from "react";
import {
    TextField,
    Select,
    MenuItem,
    InputLabel,
    FormControl,
} from "@material-ui/core";
import JalaliUtils from "@date-io/jalaali";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { Task } from "./CreateTaskDialog";
import { useSelector } from 'react-redux';
import { RootState } from "../../../store";

interface CreateTaskInfoFormProps {
    data: Task;
    onChange: (v: any) => void;
    className?: string;
}

const CreateTaskInfoForm: FC<CreateTaskInfoFormProps> = ({
    data,
    onChange,
    className,
}) => {
    const changeData = (d: Task) => {
        onChange({ ...data, ...d });
    };

    const classList = useSelector((state: RootState) => state.class.classes);
    const courseList = useSelector((state: RootState) => state.course.courses);
    const levelList = useSelector((state: RootState) => state.level.levels);

    return (
        <div
            className={
                "flex flex-col max-w-2xl w-full self-center " + (className || "")
            }
        >
            <div className="flex flex-col lg:flex-row mb-6">
                <div className="w-full">
                    <TextField
                        label="عنوان"
                        variant="filled"
                        classes={{ root: "w-full" }}
                        value={data.homework_name}
                        onChange={(e) => changeData({ homework_name: e.target.value })}
                    />
                    <div className="w-6 h-0 flex-shrink-0" />
                    <div className="w-full" />
                </div>
                <div className="w-6 h-6 flex-shrink-0" />
                <FormControl className="w-full" variant="filled">
                    <InputLabel>پایه</InputLabel>
                    <Select
                        className="w-full"
                        value={data.homeWork_level}
                        onChange={(e) => {
                            changeData({ homeWork_level: e.target.value as string });
                        }}
                    >
                        {levelList.map((l, i) => (
                            <MenuItem key={i + 1} value={l.id}>{l.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
            <div className="flex flex-col lg:flex-row mb-6">
                <FormControl className="w-full" variant="filled">
                    <InputLabel>کلاس</InputLabel>
                    <Select
                        className="w-full"
                        value={data.homework_class_id}
                        onChange={(e) => {
                            changeData({ homework_class_id: e.target.value as string });
                        }}
                    >
                        {classList.map((c, i) => (
                            <MenuItem key={i + 1} value={c.id}>{c.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <div className="w-6 h-6 flex-shrink-0" />
                <FormControl className="w-full" variant="filled">
                    <InputLabel>درس</InputLabel>
                    <Select
                        className="w-full"
                        value={data.homework_lesson_id}
                        onChange={(e) => changeData({ homework_lesson_id: e.target.value as string })}
                    >
                        {courseList.map((c, i) => (
                            <MenuItem key={i + 1} value={c.id}>{c.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
            <div className="flex flex-col lg:flex-row mb-6">
                <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa">
                    <DatePicker
                        className="w-full"
                        okLabel="تأیید"
                        variant="dialog"
                        label="تاریخ شروع"
                        cancelLabel="لغو"
                        inputVariant="filled"
                        labelFunc={(date) =>
                            date ? date.format("jYYYY/jMM/jDD") : ""
                        }
                        value={data.homework_start_date || null}
                        onChange={(v) => changeData({ homework_start_date: v?.format("YYYY/MM/DD") || "" })}
                    />
                </MuiPickersUtilsProvider>
                <div className="w-6 h-6 flex-shrink-0" />
                <div className="w-full" />
                {/* <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa">
                    <DatePicker
                        className="w-full"
                        okLabel="تأیید"
                        variant="dialog"
                        label="تاریخ پایان"
                        cancelLabel="لغو"
                        inputVariant="filled"
                        labelFunc={(date) =>
                            date ? date.format("jYYYY/jMM/jDD") : ""
                        }
                        value={data.homework_end_date || null}
                        onChange={(v) => changeData({ homework_end_date: v?.format("YYYY/MM/DD") || "" })}
                    />
                </MuiPickersUtilsProvider> */}
            </div>
            <div className="flex flex-col lg:flex-row mb-0">
                <TextField
                    label="توضیحات"
                    variant="filled"
                    multiline
                    rows={6}
                    classes={{ root: "w-full" }}
                    value={data.homework_description}
                    onChange={(e) => changeData({ homework_description: e.target.value })}
                />
            </div>
        </div>
    );
};

export default CreateTaskInfoForm;
