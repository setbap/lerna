import { Button } from "@material-ui/core";
import React, { FC, useEffect, useState } from "react";
import MyDialog from "../../utils/MyDialog";
import StepBar from "../../utils/StepBar";
import CreateTaskInfoForm from "./CreateTaskInfoForm";
import CreateTaskImageForm from "./CreateTaskImageForm";
import { v4 as uuid } from 'uuid';
import { useAxios } from "../../../contexts/AxiosContext";
import { useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';
import { RootState } from "../../../store";
import { toastErrors, toastResponseErrors } from "../../../shared/axios"

export type Task = {
    homework_id?: string,
    homework_lesson_id?: string,
    homework_class_id?: string,
    homeWork_level?: string,
    homework_teacher_id?: string,
    homework_start_date?: string,
    homework_end_date?: string,
    homework_name?: string,
    homework_description?: string,
    homework_file?: string[],
    homework_build_date?: string,
    homework_update_date?: string,
}

interface CreateTaskDialogProps {
    open: boolean;
    onClose: () => void,
    onDone?: (task: Task | null) => void
    isEdit?: boolean,
    infoData?: Task,
    files?: any[],
    onInfoDataChange?: (task: Task) => void,
    onFilesChange?: (files: any[]) => void,
}

// jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: true });

const CreateTaskDialog: FC<CreateTaskDialogProps> = ({ open, onClose, isEdit, infoData, files, onInfoDataChange, onFilesChange, onDone }) => {

    const { enqueueSnackbar } = useSnackbar();
    const axios = useAxios();

    const teacher = useSelector((state: RootState) => state.teacher.current);

    const [loading, setLoading] = useState(false)
    const [step, setStep] = useState(1)
    const [_infoData, setInfoData] = useState<Task>({})
    const [_files, setFiles] = useState<any[]>([])

    useEffect(() => {
        open && setStep(1);
    }, [open]);

    useEffect(() => {
        setInfoData(infoData || {} as Task)
    }, [open, infoData])

    useEffect(() => {
        setFiles(files || [])
    }, [open, files])

    const validValue = (v: any) => {
        return v && v !== ""
    }
    const publishTask = async () => {
        if (loading) return
        setLoading(true)

        let homework = {
            ..._infoData,
            homework_file: _files,
            homework_teacher_id: teacher.teacher_id,
            homework_id: uuid()
        } as Task

        try {
            const r = await axios.post("", {
                query: `
                    mutation{
                        AddHomeWorkForTeacher(
                            get_add_homeWork: {
                                homework_id: "${homework.homework_id}"
                                homework_lesson_id: "${homework.homework_lesson_id}"
                                homework_class_id: "${homework.homework_class_id}"
                                homeWork_level: "${homework.homeWork_level}"
                                homework_teacher_id: "${homework.homework_teacher_id}"
                                homework_start_date: "${homework.homework_start_date}"
                                homework_end_date: "${homework.homework_end_date}"
                                homework_name: "${homework.homework_name}"
                                homework_description: "${homework.homework_description}"
                                homework_file: "${homework.homework_file}"
                                homework_build_date: "${homework.homework_build_date}"
                                homework_update_date: "${homework.homework_update_date}"
                            }
                        ){
                            homework_id,
                            homework_lesson_id,
                            homework_class_id,
                            homeWork_level,
                            homework_teacher_id,
                            homework_start_date,
                            homework_end_date,
                            homework_name,
                            homework_description,
                            homework_file,
                            homework_build_date,
                            homework_update_date
                        }
                    }                      
                `
            })
            homework = r.data.data.AddHomeWorkForTeacher

            if (homework == null) {
                toastResponseErrors(r, enqueueSnackbar)
            } else {
                enqueueSnackbar("تکلیف ساخته شد", { variant: "success" })
                console.log(homework)
            }
        } catch (e) {
            toastErrors(e, enqueueSnackbar)
            homework = null
        }

        setLoading(false)
        onDone && onDone(homework)
        onClose && onClose()
    }

    const editTaskInfo = async () => {
        if (loading) return
        setLoading(true)

        let homework = {
            ..._infoData,
            homework_file: _files,
            homework_teacher_id: teacher.teacher_id,
        } as Task

        try {
            const r = await axios.post("", {
                query: `
                    mutation{
                        updateHomeWorkForTeacher(
                            input_old: {
                                homework_id: "${homework.homework_id}"
                            }, 
                            input_new: {
                                homework_id: "${homework.homework_id}"
                                homework_lesson_id: "${homework.homework_lesson_id}"
                                homework_class_id: "${homework.homework_class_id}"
                                homeWork_level: "${homework.homeWork_level}"
                                homework_teacher_id: "${homework.homework_teacher_id}"
                                homework_start_date: "${homework.homework_start_date}"
                                homework_end_date: "${homework.homework_end_date}"
                                homework_name: "${homework.homework_name}"
                                homework_description: "${homework.homework_description}"
                            }
                        ){
                            homework_id
                        }
                    }                      
                `
            })
            const t = r.data.data.updateHomeWorkForTeacher

            if (t === null) {
                toastResponseErrors(r, enqueueSnackbar)
            } else {
                enqueueSnackbar("تکلیف ویرایش شد", { variant: "success" })
                console.log(homework)
            }

        } catch (e) {
            toastErrors(e, enqueueSnackbar)
            homework = null
        }

        setLoading(false)
        onDone && onDone(homework)
        onClose && onClose()
    }

    const editTaskImage = async () => {
        if (loading) return
        setLoading(true)

        let homework = {
            ..._infoData,
            homework_file: _files,
            homework_teacher_id: teacher.teacher_id,
        } as Task

        try {
            const r = await axios.post("", {
                query: `
                    mutation{
                        updateHomeWorkForTeacher(
                            input_old: {
                                homework_id: "${homework.homework_id}"
                            }, 
                            input_new: {
                                homework_file: "${homework.homework_file}"
                            }
                        ){
                            homework_id
                        }
                    }                      
                `
            })

            const t = r.data.data.updateHomeWorkForTeacher

            if (t == null) {
                toastResponseErrors(r, enqueueSnackbar)
            } else {
                enqueueSnackbar("تکلیف ویرایش شد", { variant: "success" })
                console.log(homework)
            }

        } catch (e) {
            enqueueSnackbar(e.message, { variant: "error" })
            homework = null
        }

        setLoading(false)
        onDone && onDone(homework)
        onClose && onClose()
    }

    const _onFilesChange = (v: any[]) => {
        setFiles(v)
        onFilesChange && onFilesChange(v)
    }

    const _onInfoDataChange = (v: Task) => {
        setInfoData(v)
        onInfoDataChange && onInfoDataChange(v)
    }

    return (
        <MyDialog
            open={open}
            title={isEdit ? "ویرایش تمرین" : "ساخت تمرین"}
            loading={loading}
            onClose={onClose}
        >
            <div className="p-4 flex flex-col">
                <StepBar
                    onSelect={isEdit ? ((selectedStep) => setStep(selectedStep)) : null}
                    className="-mx-3 lg:mx-0 mt-3"
                    steps={isEdit ? ["مشخصات کلی", "فایل ها"] : ["مشخصات کلی", "فایل ها", "انتشار"]}
                    step={step} />
                {step === 1 && <CreateTaskInfoForm
                    className="mt-8"
                    data={_infoData}
                    onChange={_onInfoDataChange} />}
                {step === 2 && <CreateTaskImageForm
                    files={_files}
                    setLoading={(v) => setLoading(v)}
                    onFilesChange={_onFilesChange}
                    className="mt-3" />}
                {step === 3 && <div className="mt-16 p-12 px-4 flex items-center justify-center">
                    <Button
                        color="primary"
                        variant="contained"
                        style={{ borderRadius: "9999px", outline: "none" }}
                        size="large"
                        onClick={publishTask}>
                        <span className="px-6 py-2 font-bold text-gray-800 text-lg">انتشار</span>
                    </Button>
                </div>}
                <div className="flex mx-2 lg:mx-4 mb-1 lg:mb-2 mt-16 justify-between">
                    {(!isEdit && step > 1) && <Button onClick={() => setStep(v => v - 1)} style={{ borderRadius: "9999px", outline: "none" }} color="primary">
                        <span className="px-2 font-bold text-gray-800">قبلی</span>
                    </Button>}
                    <div className="flex-grow" />
                    {(isEdit || step < 3) && <Button
                        onClick={() => {
                            if (isEdit) {
                                if (step === 1) editTaskInfo()
                                else if (step === 2) editTaskImage()
                            } else {
                                if (step === 1) {
                                    if (
                                        !validValue(_infoData.homework_start_date) ||
                                        !validValue(_infoData.homework_end_date) ||
                                        !validValue(_infoData.homework_lesson_id) ||
                                        !validValue(_infoData.homework_class_id) ||
                                        !validValue(_infoData.homeWork_level) ||
                                        !validValue(_infoData.homework_name) ||
                                        !validValue(_infoData.homework_description)
                                    ) {
                                        enqueueSnackbar("لطفآ تمام فیلد ها را پر کنید", { variant: "error" })
                                        return
                                    } else {
                                        setStep(v => v + 1)
                                    }
                                } else if (step === 2) {
                                    setStep(v => v + 1)
                                }
                            }
                        }}
                        variant="contained" style={{ borderRadius: "9999px", outline: "none" }} color="primary">
                        <span className="px-2 font-bold text-gray-800">
                            {isEdit ? 'ویرایش' : 'بعدی'}
                        </span>
                    </Button>}
                </div>
            </div>
        </MyDialog>
    )
}

export default CreateTaskDialog;
