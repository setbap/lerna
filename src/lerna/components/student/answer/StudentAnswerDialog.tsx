import { Button, TextField } from "@material-ui/core";
import React, { FC, useEffect, useState } from "react";
import MyDialog from "../../utils/MyDialog";
import CommentIcon from '@material-ui/icons/Comment';
import FileDrop from "../../utils/FileDrop";
import { useAxios } from "../../../contexts/AxiosContext";
import { useSnackbar } from "notistack";
import { TaskAnswer } from "../../../pages/teacher/task";
import { v4 as uuid } from "uuid"
import { useSelector } from "react-redux";
import { RootState } from "../../../store";
import { toastResponseErrors } from "../../../shared/axios";

interface StudentAnswerDialogProps {
	open: boolean
	onClose: () => void
	answer?: TaskAnswer
	onDone?: (answer: TaskAnswer | null) => void
}

const StudentAnswerDialog: FC<StudentAnswerDialogProps> = ({ open, onClose, answer, onDone }) => {

	const student = useSelector((state: RootState) => state.student.current);

	const [comment, setComment] = useState("")
	const [file, setFile] = useState<string | null>(null)
	const [loading, setLoading] = useState(false)

	const { enqueueSnackbar } = useSnackbar();
	const axios = useAxios();

	const validValue = (v: any) => {
		return v && v !== ""
	}

	useEffect(() => {
		setComment(answer?.homeworkSolved_commentsStudent || "")
	}, [open, answer?.homeworkSolved_commentsStudent])

	const save = async () => {
		if (!validValue(comment) || !validValue(file)) {
			enqueueSnackbar("لطفآ تمام فیلد ها را پر کنید", { variant: "error" })
			return
		}

		if (loading) return
		setLoading(true)

		let a = {
			homeworkSolved_id: uuid(),
			homeworkSolved_homework_id: answer?.homeworkSolved_homework_id || "",
			homeworkSolved_student_id: student.student_id || "",
			homeworkSolved_commentsTeacher: answer?.homeworkSolved_commentsTeacher || "",
			homeworkSolved_commentsStudent: comment || "",
			homeworkSolved_file: file || answer?.homeworkSolved_file || "",
			homeworkSolved_build_date: answer?.homeworkSolved_commentsStudent || "",
			homeworkSolved_update_date: answer?.homeworkSolved_commentsStudent || "",
		} as TaskAnswer

		try {
			const r = await axios.post("", {
				query: `   
					mutation {
						AddHomeWorkForStudent(get_add_homeWork: {
							homeworkSolved_id: "${a.homeworkSolved_id}",
							homeworkSolved_homework_id: "${a.homeworkSolved_homework_id}",
							homeworkSolved_student_id: "${a.homeworkSolved_student_id}",
							homeworkSolved_commentsTeacher: "${a.homeworkSolved_commentsTeacher}",
							homeworkSolved_commentsStudent: "${a.homeworkSolved_commentsStudent}",
							homeworkSolved_file: "${a.homeworkSolved_file}",
							homeworkSolved_build_date: "${a.homeworkSolved_build_date}",
							homeworkSolved_update_date: "${a.homeworkSolved_update_date}"
						}) {
							homeworkSolved_id
							homeworkSolved_homework_id
							homeworkSolved_student_id
							homeworkSolved_commentsTeacher
							homeworkSolved_commentsStudent
							homeworkSolved_file
							homeworkSolved_build_date
							homeworkSolved_update_date
						}
					}        
          `
			})

			a = r.data.data.AddHomeWorkForStudent

			if (a == null) {
				toastResponseErrors(r, enqueueSnackbar)
			} else {
				enqueueSnackbar("جواب ارسال شد", { variant: "success" })
				console.log(a)
				onDone && onDone(a)
				onClose && onClose()
			}


		} catch (e) {
			enqueueSnackbar(e.message, { variant: "error" })
			a = null
		}

		setLoading(false)
	}

	return (
		<MyDialog
			open={open}
			title="افزودن عکس"
			loading={loading}
			onClose={onClose}
		>
			<div className="p-4 flex flex-col">
				{!file ? (
					<FileDrop
						className="mt-0 lg:mt-4 mb-4 mx-0 lg:mx-4"
						text="عکس را در اینجا رها کنید"
						onStart={() => setLoading(true)}
						onChange={async f => {
							setLoading(false)
							setFile(f)
						}} />
				) : (
						<div className="flex flex-col items-center">
							<div className="mt-0 lg:mt-4 mb-4 relative w-full max-w-md bg-natural-light rounded-2xl border border-natural-light overflow-hidden self-center">
								<div className="pt-9/16 w-full">
									<img
										className="absolute left-0 bg-white right-0 top-0 bottom-0 w-full h-full object-cover"
										src={file}
										alt=""
									/>
								</div>
							</div>
							<Button variant="contained"
								className="bg-red-500 text-white mb-4"
								onClick={() => setFile(null)}>حدف عکس</Button>
						</div>
					)}

				{answer?.homeworkSolved_commentsTeacher && answer?.homeworkSolved_commentsTeacher !== "" &&
					<div className="mt-4 mx-0 lg:mx-4">
						<div className="flex flex-row items-center">
							<CommentIcon style={{ fontSize: 28, color: "#4CAF50" }} />
							<span className="text-natural-dark font-bold text-lg ms-3">
								نظر استاد
            			</span>
						</div>
						<div className="flex flex-col mt-1">
							<div className="h-8 relative overflow-hidden z-10">
								<div className="absolute top-0 mt-4 start-0 ms-12 w-16 h-16 bg-natural-light transform rotate-45 " />
							</div>
							<div
								className="bg-natural-light p-4 rounded-t-2xl"
								style={{ minHeight: "140px" }}
							>
								<span className="text-natural-dark">{answer?.homeworkSolved_commentsTeacher}</span>
							</div>
						</div>
					</div>
				}
				<div className="mt-4 mx-0 lg:mx-4">
					<div className="flex flex-row items-center">
						<CommentIcon style={{ fontSize: 28, color: "#FF9800" }} />
						<span className="text-natural-dark font-bold text-lg ms-3">نظر دانش‌آموز</span>
					</div>
					<div className="flex flex-col mt-1">
						<div className="h-8 relative overflow-hidden z-10">
							<div className="absolute top-0 mt-4 start-0 ms-12 w-16 h-16 bg-natural-light transform rotate-45 " />
						</div>
						<TextField multiline variant="filled"
							label="نظر دانش‌آموز"
							classes={{
								root: ""
							}}
							InputProps={{
								classes: {
									root: "rounded-t-2xl shadow-lg",
								},

							}}
							inputProps={{
								style: {
									minHeight: "103px"
								}
							}}
							value={comment}
							onChange={(e) => (setComment(e.target.value || ""))} />
					</div>
				</div>
				<div className="flex mx-2 lg:mx-4 mb-1 lg:mb-2 mt-16 justify-between">
					<div className="flex-grow" />
					<Button
						onClick={save}
						variant="contained" style={{ borderRadius: "9999px", outline: "none" }} color="primary">
						<span className="px-2 font-bold text-gray-800">
							افزودن
						</span>
					</Button>
				</div>
			</div>
		</MyDialog >
	)
}

export default StudentAnswerDialog