import { Button, Select, MenuItem, InputLabel, FormControl } from "@material-ui/core";
import React, { FC, useEffect, useState } from "react";
import MyDialog from "../../utils/MyDialog";
import JalaliUtils from "@date-io/jalaali";
import { useSelector } from 'react-redux';
import { RootState } from "../../../store";

import {
	DatePicker,
	MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import moment from "moment-jalaali";

type StudentFilterType = {
	date?: string | null
	lessonId?: string | null
}

interface StudentFilterDialogProps {
	open: boolean
	onClose: () => void,
	filters?: StudentFilterType,
	onChange?: (filters: StudentFilterType) => void
}

const StudentFilterDialog: FC<StudentFilterDialogProps> = ({ open, onClose, filters, onChange }) => {

	const courseList = useSelector((state: RootState) => state.course.courses);

	const [data, setData] = useState({} as StudentFilterType)

	const changeData = (d: StudentFilterType) => {
		setData({ ...data, ...d })
	}

	useEffect(() => {
		setData(filters || {} as StudentFilterType)
	}, [open, filters])

	const submit = () => {
		console.log("submited")
		onChange && onChange(data)
		onClose && onClose()
	}

	const clear = () => {
		console.log("cleared")
		setData({ date: moment().format("YYYY/MM/DD") } as StudentFilterType)
	}

	return (
		<MyDialog
			open={open}
			title="فیلتر ها"
			size="sm"
			onClose={onClose}
		>
			<div className="p-4 flex flex-col">
				<div className="px-0 lg:px-2 pt-0 lg:pt-2">
					<FormControl className="w-full mb-6" variant="filled">
						<InputLabel>درس</InputLabel>
						<Select
							className="w-full"
							value={data.lessonId || null}
							defaultChecked={false}
							onChange={(e) => changeData({ lessonId: e.target.value as any })}
						>
							{courseList.map((c, i) => (
								<MenuItem key={i + 1} value={c.id}>{c.name}</MenuItem>
							))}
						</Select>
					</FormControl>
					<MuiPickersUtilsProvider utils={JalaliUtils} locale="fa">
						<DatePicker
							className="w-full"
							okLabel="تأیید"
							variant="dialog"
							label="تاریخ"
							cancelLabel="لغو"
							inputVariant="filled"
							labelFunc={(date) =>
								date ? date.format("jYYYY/jMM/jDD") : ""
							}
							value={data.date || null}
							onChange={(v) => changeData({ date: v?.format("YYYY/MM/DD") || null })}
						/>
					</MuiPickersUtilsProvider>
				</div>
				<div className="flex mx-2 lg:mx-4 mb-1 lg:mb-2 mt-16 justify-between">
					{(Object.keys(data).length > 0) && <Button onClick={clear}
						style={{ borderRadius: "9999px", outline: "none" }}
						color="default">
						<span className="px-2 font-bold text-red-500">پاک کردن</span>
					</Button>}
					<div className="flex-grow" />
					<Button
						onClick={submit}
						variant="contained" style={{ borderRadius: "9999px", outline: "none" }} color="primary">
						<span className="px-2 font-bold text-gray-800">
							اعمال
						</span>
					</Button>
				</div>
			</div>
		</MyDialog >
	)
}

export default StudentFilterDialog