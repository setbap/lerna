import React, { FC } from "react";
import { Dialog, Button, CircularProgress } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

interface MyDialogProps {
	open: boolean;
	onClose: () => void;
	size?: "sm" | "md" | "lg" | "xl";
	title: string;
	loading?: boolean
}

const MyDialog: FC<MyDialogProps> = (props) => {
	const { onClose, open, size, title, loading } = props;

	return (
		<Dialog
			PaperProps={{
				style: { borderRadius: 16 },
			}}
			scroll="body"
			classes={{}}
			fullWidth={true}
			maxWidth={size || "md"}
			onClose={onClose}
			open={open}
		>
			<div className="relative"
				style={loading ? { filter: "blur(3px)" } : {}}>
				<div className="flex flex-row">
					<span className="mx-5 lg:mx-8 my-4 lg:my-5 text-lg lg:text-xl text-grey-800 flex-grow">
						{title}
					</span>
					<span className="bg-grey-300 w-px flex-shrink-0" />
					<Button onClick={onClose} style={{ borderRadius: 0, outline: "none" }}>
						<CloseIcon fontSize="large" className="text-grey-700 mx-0 lg:mx-2" />
					</Button>
				</div>
				<div className="bg-grey-300 h-px flex-shrink-0" />
				{props.children}
			</div>
			{loading && (
				<div className="absolute w-full h-full left-0 right-0 top-0 bottom-0 bg-white bg-opacity-50 flex items-center justify-center"
					style={{}}>
					<CircularProgress color="primary" size={82} />
				</div>
			)}
		</Dialog>
	);
};

export default MyDialog;
