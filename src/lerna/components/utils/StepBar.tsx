import React, { FC } from "react";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";

interface StepBarProps {
  steps: string[];
  step: number;
  onSelect?: ((step: number) => void) | null;
  className?: string;
}

const StepBar: FC<StepBarProps> = ({ steps, step, onSelect, className }) => {
  return (
    <div className={className}>
      <Stepper alternativeLabel activeStep={step - 1} classes={{ root: "p-0" }}>
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel
              onClick={() => onSelect && onSelect(index + 1)}
              classes={{
                label: "font-bold text-natural-dark",
              }}
              className={onSelect ? "cursor-pointer" : ""}
            >
              {label}
            </StepLabel>
          </Step>
        ))}
      </Stepper>
    </div>
  );
};

export default StepBar;
