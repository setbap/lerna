import React, { FC } from "react";
import ScrollContainer from "react-indiana-drag-scroll";
import FileIcon from "@material-ui/icons/InsertDriveFile";

type FileListProps = {
    files: any[];
    className?: string;
    size?: "small" | "large";
    removable?: boolean;
    onRemove?: (file: any) => void;
    onDownload?: (file: any) => void;
};

const FileList: FC<FileListProps> = ({
    className,
    size,
    removable,
    files,
    onRemove,
    onDownload,
}) => {
    // const config = () => {
    //   if (size === "small") {
    //     return {
    //       itemClass: "w-32",
    //       iconClass: "",
    //     };
    //   } else {
    //     return {
    //       itemClass: "w-32",
    //       iconClass: "",
    //     };
    //   }
    // };

    return (
        <ScrollContainer
            style={{ borderColor: "#C3D1E0" }}
            className={
                "bg-natural-light border overflow-x-scroll " +
                (size === "large" ? "rounded-2xl " : "rounded-md ") +
                className
            }
        >
            <div
                className={"flex flex-row p-3 " + (size === "large" ? "p-3 " : "p-2 ")}
            >
                {!files?.length && (
                    <div className="flex flex-row flex-shrink-0 invisible">
                        <div
                            style={{ borderColor: "#C3D1E0" }}
                            className={
                                "relative flex-shrink-0 overflow-hidden border bg-white " +
                                (size === "large"
                                    ? "w-28 lg:w-32 rounded-xl "
                                    : "w-20 rounded-md ")
                            }
                        >
                            <div className="pt-1/1 relative">
                                <div className="absolute left-0 right-0 top-0 bottom-0 w-full h-full flex items-center justify-center">
                                </div>
                            </div>
                        </div>
                        <div
                            className={
                                "w-3 flex-shrink-0 " + (size === "large" ? "w-3 " : "w-2 ")
                            }
                        />
                    </div>
                )}
                {files?.map((file, index) => (
                    <div key={index} className="flex flex-row flex-shrink-0">
                        <div
                            style={{ borderColor: "#C3D1E0" }}
                            className={
                                "relative flex-shrink-0 overflow-hidden border bg-white " +
                                (size === "large"
                                    ? "w-28 lg:w-32 rounded-xl "
                                    : "w-20 rounded-md ")
                            }
                        >
                            <div className="pt-1/1 relative">
                                <div className="absolute left-0 right-0 top-0 bottom-0 w-full h-full flex items-center justify-center">
                                    <FileIcon
                                        style={{
                                            fontSize: size === "large" ? 66 : 46,
                                            color: "#AD4DA9",
                                        }}
                                    />
                                </div>
                                <div className="absolute opacity-0 hover:opacity-100 transition-opacity duration-300 left-0 right-0 top-0 bottom-0 w-full h-full flex flex-col items-center justify-evenly bg-white bg-opacity-75">
                                    <a
                                        className="text-natural-dark underline cursor-pointer p-2"
                                        href={file}
                                        onClick={() => onDownload && onDownload(file)}
                                    >
                                        دانلود
									</a>
                                    {removable && (
                                        <span
                                            className="text-red-500 underline cursor-pointer p-2"
                                            onClick={() => onRemove && onRemove(file)}
                                        >
                                            حذف
                                        </span>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div
                            className={
                                "w-3 flex-shrink-0 " + (size === "large" ? "w-3 " : "w-2 ")
                            }
                        />
                    </div>
                ))}
            </div>
        </ScrollContainer>
    );
};

export default FileList;
