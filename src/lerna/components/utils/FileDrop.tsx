import React, { FC, useRef } from "react";
import AttachmentIcon from '@material-ui/icons/Attachment';
import { useSnackbar } from "notistack";
import { uploadFile } from "../../shared/upload"

interface FileDropProps {
	className?: string,
	onChange?: (url: string | null) => Promise<any>
	onStart?: () => void
	text?: string
}

const FileDrop: FC<FileDropProps> = ({ onChange, className, text, onStart }) => {

	const { enqueueSnackbar } = useSnackbar();

	const fileInput = useRef<HTMLInputElement | null>()

	const handleFileSelect = async (e: any) => {
		const files = e.target.files || e.dataTransfer.files
		if (!files.length) {
			onChange && await onChange(null)
			return
		}
		const file = files[0]
		await upload(file)
		await clearFile()
	}

	const upload = async (file: File) => {
		onStart && onStart()
		uploadFile(file, (e) => {
			console.log(e)
		}).then(async (r) => {
			console.log(r)
			enqueueSnackbar("فایل آپلود شد", { variant: "success" })
			onChange && await onChange(r.url)
		}).catch(async (e) => {
			console.log(e)
			enqueueSnackbar(e.message, { variant: "error" })
			onChange && await onChange(null)
		})
	}

	const clearFile = async () => {
		if (fileInput.current) {
			fileInput.current.value = ''
		}
	}

	return (
		<label
			style={{ borderColor: "#C3D1E0" }}
			className={
				"ripple-bg-natural-light border rounded-2xl flex flex-col items-center py-8 px-4 cursor-pointer relative overflow-hidden " +
				className
			}>
			<AttachmentIcon style={{ fontSize: 60, color: "#334D6E" }} className="mb-5" />
			<span className="ripple-text-natural-dark lg:text-lg text-center mb-4">{text || "فایل جدید را در اینجا رها کنید"}</span>
			<input ref={v => fileInput.current = v} type="file" onChange={handleFileSelect} className="w-full h-full left-0 right-0 top-0 bottom-0 opacity-0 transform scale-150 absolute cursor-pointer" tabIndex={-1} />
		</label>
	)
}

export default FileDrop