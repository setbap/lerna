import React, { FC, Suspense } from "react";
import { Button, CircularProgress } from "@material-ui/core";
import { PanelContainer } from "../config"

type RootContainerType = {
	loading?: boolean,
	failed?: boolean,
	onTryAgain?: () => void
}

const RootContainer: FC<RootContainerType> = (props) => {

	const { loading, failed, onTryAgain } = props;

	const LoadingView = () => (
		<div className="absolute top-0 left-0 right-0 max-h-screen h-full w-full overflow-hidden bg-white bg-opacity-50 flex items-center justify-center">
			<CircularProgress color="primary" size={82} />
		</div>
	)

	return (
		<div className={"relative max-w-screen min-h-screen"}>
			<Suspense fallback={<LoadingView />} >
				<div className="flex-grow relative overflow-hidden"
					style={loading || failed ? { filter: "blur(3px)" } : {}}>
					<PanelContainer>
						{props.children}
					</PanelContainer>
				</div>
			</Suspense>
			{loading && (<LoadingView />)}
			{!loading && failed && (
				<>
					<div className="absolute top-0 left-0 right-0 max-h-screen h-full w-full overflow-hidden bg-white bg-opacity-50 flex items-center justify-center">
						<Button variant="contained" color="primary" onClick={onTryAgain}>تلاش دوباره</Button>
					</div>
				</>
			)}
		</div>
	);
};

export default RootContainer;
