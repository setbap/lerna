import { lazy } from "react";

// GraphQL server url
export const ApiUrl = "https://moored-fantastic-turret.glitch.me"; // without slash

// upload server url
export const UploadUrl = "https://raymon-tech.ir"; // without slash

// base url for uploaded files
export const FileUploadedUrl =
  "https://kamal-chat.s3.ir-thr-at1.arvanstorage.com/"; // with slash

// base url for lerna project
export const BaseUrl = "/lerna"; // without slash
//export const BaseUrl = "/lerna";

// your project panel template
export const PanelContainer = lazy(() => import("../PanelContainer"));
