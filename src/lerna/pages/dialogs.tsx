
import React, { FC, useRef, useState } from "react";
import Button from "@material-ui/core/Button";
import CreateTaskDialog from "../components/teacher/task/CreateTaskDialog";
import TeacherAnswerDialog from "../components/teacher/answer/TeacherAnswerDialog";
import StudentAnswerDialog from "../components/student/answer/StudentAnswerDialog";
import StudentFilterDialog from "../components/student/filter/StudentFilterDialog";
import TeacherFilterDialog from "../components/teacher/filter/TeacherFilterDialog";
import RootContainer from "../components/RootContainer";

const DialogsPage: FC = () => {

	const image = useRef<HTMLImageElement>()

	const [openCreateTaskDialog, setOpenCreateTaskDialog] = useState(false);
	const [openEditTaskDialog, setOpenEditTaskDialog] = useState(false);
	const [openTeacherAnswerDialog, setOpenTeacherAnswerDialog] = useState(false);
	const [openStudentAnswerDialog, setOpenStudentAnswerDialog] = useState(false);
	const [openStudentFilterDialog, setOpenStudentFilterDialog] = useState(false);
	const [openTeacherFilterDialog, setOpenTeacherFilterDialog] = useState(false);

	return (
		<RootContainer>
			<div className="flex flex-col justify-evenly items-center p-4 min-h-full">
				<img ref={v => image.current = v} alt="" width="400" height="auto" className="border border-black m-4" />
				<Button
					variant="contained"
					className="m-4"
					color="primary"
					style={{ outline: "none" }}
					onClick={() => setOpenCreateTaskDialog(true)}
				>
					ساخت تمرین
        	</Button>
				<Button
					variant="contained"
					className="m-4"
					color="primary"
					style={{ outline: "none" }}
					onClick={() => setOpenEditTaskDialog(true)}
				>
					ویرایش تمرین
        	</Button>
				<Button
					variant="contained"
					className="m-4"
					color="primary"
					style={{ outline: "none" }}
					onClick={() => setOpenTeacherAnswerDialog(true)}
				>
					بررسی جواب
        	</Button>
				<Button
					variant="contained"
					className="m-4"
					color="primary"
					style={{ outline: "none" }}
					onClick={() => setOpenStudentAnswerDialog(true)}
				>
					افزودن عکس
        	</Button>
				<Button
					variant="contained"
					className="m-4"
					color="primary"
					style={{ outline: "none" }}
					onClick={() => setOpenStudentFilterDialog(true)}
				>
					فیلتر دانش آموزان
        	</Button>
				<Button
					variant="contained"
					className="m-4"
					color="primary"
					style={{ outline: "none" }}
					onClick={() => setOpenTeacherFilterDialog(true)}
				>
					فیلتر استاد
        	</Button>
				<CreateTaskDialog
					open={openCreateTaskDialog}
					onClose={() => setOpenCreateTaskDialog(false)}
					onDone={(task) => console.log(task)}
					infoData={{}}
					files={Array(8).fill(1)}
					onFilesChange={(files) => { }}
					onInfoDataChange={(info) => { }} />
				<CreateTaskDialog
					open={openEditTaskDialog}
					isEdit
					onClose={() => setOpenEditTaskDialog(false)}
					onDone={(task) => console.log(task)}
					infoData={{}}
					files={Array(8).fill(1)}
					onFilesChange={(files) => { }}
					onInfoDataChange={(info) => { }} />
				<TeacherAnswerDialog
					open={openTeacherAnswerDialog}
					onClose={() => setOpenTeacherAnswerDialog(false)}
					answer={{
						homeworkSolved_build_date: "",
						homeworkSolved_commentsStudent: "",
						homeworkSolved_commentsTeacher: "",
						homeworkSolved_homework_id: "",
						homeworkSolved_update_date: "",
					}} />
				<StudentAnswerDialog
					open={openStudentAnswerDialog}
					onClose={() => setOpenStudentAnswerDialog(false)} />
				<StudentFilterDialog
					open={openStudentFilterDialog}
					onClose={() => setOpenStudentFilterDialog(false)}
					filters={{}}
					onChange={filters => { }} />
				<TeacherFilterDialog
					open={openTeacherFilterDialog}
					onClose={() => setOpenTeacherFilterDialog(false)}
					filters={{}}
					onChange={filters => { }} />
			</div>
		</RootContainer>
	);
};

export default DialogsPage;
