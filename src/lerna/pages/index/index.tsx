import { Box, Button } from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";
import React, { FC } from "react";
import { StudentPageLink, TeacherPageLink, TeacherTaskPageLink, DialogsLink } from "../../shared/links";
import RootContainer from "../../components/RootContainer";

// interface IMainIndex {}

const MainIndex: FC<{}> = () => {
    return (
        <RootContainer>
            <Box
                display={"flex"}
                flexDirection={"column"}
                justifyContent={"center"}
                alignItems={"center"}
                height={"100vh"}
            >
                <Box>
                    <Button
                        component={RouterLink}
                        to={TeacherPageLink}
                        variant="outlined"
                        color="primary"
                    >
                        معلم
				</Button>
                </Box>
                <Box m={4}>
                    <Button
                        component={RouterLink}
                        to={StudentPageLink}
                        variant="outlined"
                        color="secondary"
                    >
                        دانش آموز
				</Button>
                </Box>
                <Box>
                    <Button
                        component={RouterLink}
                        to={TeacherTaskPageLink}
                        variant="outlined"
                        color="secondary"
                    >
                        تکلیف
				</Button>

                </Box>
                <Box m={4}>
                    <Button
                        component={RouterLink}
                        to={DialogsLink}
                        variant="outlined"
                        color="secondary"
                    >
                        دیالوگ ها
					</Button>
                </Box>
            </Box>
        </RootContainer>
    );
};

export default MainIndex;
