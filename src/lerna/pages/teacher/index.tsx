import React, { useEffect, useState } from "react";
import Container from "@material-ui/core/Container";
import { ExerciseCard } from "../../shared/ExerciseCard";
import FilterListIcon from "@material-ui/icons/FilterList";
import SearchIcon from "@material-ui/icons/Search";
import IconButton from "@material-ui/core/IconButton";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { teacherTaskPageLinkSelect } from "../../shared/links";
import TeacherFilterDialog from "../../components/teacher/filter/TeacherFilterDialog";
import CreateTaskDialog, { Task } from "../../components/teacher/task/CreateTaskDialog";
import { useAxios } from "../../contexts/AxiosContext";
import { useSnackbar } from 'notistack';
import moment from "moment-jalaali";
import { useSelector } from 'react-redux';
import { RootState } from "../../store";
import RootContainer from "../../components/RootContainer";
import { fixHomeworks, toastResponseErrors } from "../../shared/axios";

const TeacherPage = () => {

    const axios = useAxios()
    const { enqueueSnackbar } = useSnackbar();

    const teacher = useSelector((state: RootState) => state.teacher.current);
    const courseList = useSelector((state: RootState) => state.course.courses);
    const levelList = useSelector((state: RootState) => state.level.levels);
    const classList = useSelector((state: RootState) => state.class.classes);

    const [filter, setFilter] = useState({
        date: moment().format("YYYY/MM/DD"),
        classId: null,
        gradeId: null,
        lessonId: null,
    })
    const [searchText, setSearchText] = useState("")
    const [homeworks, setHomeworks] = useState<Task[]>([])
    const [loading, setLoading] = useState<{ loading?: boolean, fetched?: boolean }>({ loading: false, fetched: false })
    const [openTeacherFilterDialog, setOpenTeacherFilterDialog] = useState(false);
    const [openEditTaskDialog, setOpenEditTaskDialog] = useState(false);
    const [openCreateTaskDialog, setOpenCreateTaskDialog] = useState(false);
    const [fetchTrigger, setFetchTrigger] = useState(0)

    const [targetInfoData, setTargetInfoData] = useState<Task | null>(null)
    const [targetFiles, setTargetFiles] = useState<string[] | null>(null)

    useEffect(() => {
        fetch()
        // eslint-disable-next-line
    }, [fetchTrigger])

    const filterHomeworks = () => {
        return homeworks
            ?.filter(h => !filter.lessonId || h.homework_lesson_id === filter.lessonId)
            ?.filter(h => !filter.gradeId || h.homeWork_level === filter.gradeId)
            .filter(h => !filter.classId || h.homework_class_id === filter.classId)
            .filter(h => !searchText || searchText === "" || h.homework_name.includes(searchText)) || []
    }

    const fetch = async () => {
        if (loading.loading) return
        setLoading(v => ({ ...v, loading: true }))

        try {
            const r = await axios.post("", {
                query: `
                    mutation {
                        getListHomeWorkWithDate (
                            get_list_homework: {
                                homework_start_date: "${filter.date}",
                                homework_teacher_id: "${teacher.teacher_id}"
                            }
                        ) {
                            homework_id
                            homework_lesson_id
                            homework_class_id
                            homeWork_level
                            homework_teacher_id
                            homework_start_date
                            homework_end_date
                            homework_name
                            homework_description
                            homework_file
                            homework_build_date
                            homework_update_date
                        }
                    }              
					`
            })
            console.log(r.data)
            const hs = fixHomeworks(r.data.data.getListHomeWorkWithDate)
            if (hs === null) {
                toastResponseErrors(r, enqueueSnackbar)
                setLoading({ loading: false, fetched: false })
            } else {
                setHomeworks(hs)
                setLoading({ loading: false, fetched: true })
            }
        } catch (e) {
            enqueueSnackbar(e.message, { variant: "error" })
            setLoading({ loading: false, fetched: false })
        }
    }

    return (
        <RootContainer
            loading={loading.loading}
            failed={!loading.fetched}
            onTryAgain={() => setFetchTrigger(v => v + 1)}>
            <Container maxWidth="lg" className="p-4">
                <div className="mb-6 justify-center items-center mt-2 mx-auto  max-w-xl flex">
                    <IconButton
                        className="me-3 ms-2 focus:outline-none"
                        aria-label="chnage filter"
                        onClick={() => setOpenTeacherFilterDialog(true)}
                    >
                        <FilterListIcon />
                    </IconButton>
                    <TeacherFilterDialog
                        open={openTeacherFilterDialog}
                        onClose={() => setOpenTeacherFilterDialog(false)}
                        filters={filter}
                        onChange={filters => {
                            const needFetch = filters.date !== filter.date
                            setFilter(filters as any)
                            needFetch && setFetchTrigger(v => v + 1)
                        }} />
                    <label className=" border-grey-300 overflow-hidden bg-white  items-center border rounded-full p-1 flex-1 flex">
                        <input
                            className="flex-1 px-2 py-2 bg-transparent outline-none"
                            type="text"
                            value={searchText}
                            onChange={(v) => (setSearchText(v.target.value))}
                        />

                        <IconButton
                            className=" focus:outline-none"
                            aria-label="chnage filter"
                            size="small"
                        >
                            <SearchIcon className="m-1" />
                        </IconButton>
                    </label>
                </div>
                <div className="grid grid-cols-1  gap-6 md:grid-cols-2 ">
                    {filterHomeworks().map((homework, index) => (
                        <ExerciseCard
                            key={index}
                            onClickEditButton={() => {
                                setTargetInfoData(homework)
                                setTargetFiles(homework.homework_file)
                                setOpenEditTaskDialog(true)
                            }}
                            startDate={homework.homework_start_date ? moment(homework.homework_start_date, "YYYY/MM/DD").toDate() : null}
                            endDate={homework.homework_end_date ? moment(homework.homework_end_date, "YYYY/MM/DD").toDate() : null}
                            serverTime={new Date("2020")}
                            nameOfClass={
                                "کلاس " +
                                (levelList?.find(c => c.id === homework?.homeWork_level)?.name || "نامشخص") +
                                " " +
                                (classList?.find(c => c.id === homework?.homework_class_id)?.name || "")
                                + " - " +
                                "درس " +
                                (courseList?.find(c => c.id === homework?.homework_lesson_id)?.name || "نامشخص")
                            }
                            nameOfExercise={homework.homework_name}
                            selectedLink={teacherTaskPageLinkSelect(homework?.homework_id)}
                            viewer="teacher"
                            anserwerd
                        />
                    ))}
                </div>
                <Fab
                    onClick={() => setOpenCreateTaskDialog(true)}
                    className="fixed  bg-success text-white focus:outline-none z-50 left-0 bottom-0 me-4 mb-4"
                    variant="extended"
                >
                    <AddIcon className="me-1" />
                    <span className="ms-1">افزودن تکلیف</span>
                </Fab>
                <CreateTaskDialog
                    open={openCreateTaskDialog}
                    onClose={() => setOpenCreateTaskDialog(false)}
                    onDone={(task) => {
                        if (!task) return
                        setFetchTrigger(v => v + 1)
                    }} />
                <CreateTaskDialog
                    open={openEditTaskDialog}
                    isEdit
                    onDone={(task) => {
                        if (!task) return
                        setHomeworks(v => v.map(t => t.homework_id === task.homework_id ? task : t))
                    }}
                    infoData={targetInfoData}
                    files={targetFiles}
                    onClose={() => setOpenEditTaskDialog(false)} />
            </Container>
        </RootContainer>
    );
};

export default TeacherPage;
