import React, { FC, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import Drawer from "@material-ui/core/Drawer";
import { StudentList } from "../../../shared/StudentList";
import Button from "@material-ui/core/Button";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import Fab from "@material-ui/core/Fab";
import ExerciseDetail from "../../../shared/ExerciseDetail";
import Divider from "@material-ui/core/Divider";
import { SelectStudetnBox } from "../../../shared/SelectStudetnBox";
import { ExerciseSolution } from "../../../shared/ExerciseSolution";
import RootContainer from "../../../components/RootContainer";
import { Student } from "../../../store/studentSlice";
import { useAxios } from "../../../contexts/AxiosContext";
import { useSnackbar } from "notistack";
import { Task } from "../../../components/teacher/task/CreateTaskDialog";
import { CircularProgress } from "@material-ui/core";
import { fixHomework } from "../../../shared/axios";
import { teacherTaskPageLinkSelect } from "../../../shared/links";

export type TaskAnswer = {
	homeworkSolved_id?: string
	homeworkSolved_homework_id?: string
	homeworkSolved_student_id?: string
	homeworkSolved_commentsTeacher?: string
	homeworkSolved_commentsStudent?: string
	homeworkSolved_file?: string
	homeworkSolved_build_date?: string
	homeworkSolved_update_date?: string
}

type UrlType = {
	id: string;
	studentId: string;
};

const TeacherTaskPage: FC = () => {
	let { id, studentId } = useParams<UrlType>();
	const [drawerOpen, setDrawerOpen] = useState(false);
	const history = useHistory();

	const axios = useAxios()
	const { enqueueSnackbar } = useSnackbar();

	const [loading, setLoading] = useState({ loading: false, fetched: false })
	const [fetchTrigger, setFetchTrigger] = useState(0)
	const [homework, setHomework] = useState<Task>({})
	const [students, setStudents] = useState<Student[]>([])
	const [studentLoading, setStudentLoading] = useState({ loading: false, fetched: false });
	const [answers, setAnswers] = useState<TaskAnswer[]>()

	const toggleDrawer = () => {
		setDrawerOpen((val) => !val);
	};

	const student = () => {
		return students.find(s => s.student_id === studentId)
	}

	useEffect(() => {
		getStudentAnswers()
		// eslint-disable-next-line
	}, [studentId])

	useEffect(() => {
		fetch()
		// eslint-disable-next-line
	}, [fetchTrigger])

	const fetch = async () => {
		if (loading.loading) return
		setLoading(v => ({ ...v, loading: true }))

		try {
			let r = await axios.post("", {
				query: `
                    mutation {
                        getHomeWorkInfo(get_list_homework: { homework_id: "${id}" }) {
							homework_id
							homework_lesson_id
							homework_class_id
							homeWork_level
							homework_teacher_id
							homework_start_date
							homework_end_date
							homework_name
							homework_description
							homework_file
							homework_build_date
							homework_update_date
						}
                    }              
				`
			})

			console.log(r.data)

			let h = r.data.data.getHomeWorkInfo
			if (h.length === 0) {
				// eslint-disable-next-line
				throw { message: "تکلیف پیدا نشد" }
			} else {
				h = h[0]
			}
			h = fixHomework(h)
			setHomework(h)

			r = await axios.post("", {
				query: `
					mutation {
						getListStudents(
							student_input: {
								level: "${h.homeWork_level}"
								class_name: "${h.homework_class_id}"
							}
						) {
							student_id
							student_name
							student_surname
						}
					}              
				`
			})

			setStudents(r.data.data.getListStudents)

			setLoading({ loading: false, fetched: true })
		} catch (e) {
			enqueueSnackbar(e.message, { variant: "error" })
			setLoading({ loading: false, fetched: false })
		}
	}

	const getStudents = async (level, class_name) => {
		setLoading(v => ({ ...v, loading: true }))
		try {
			const r = await axios.post("", {
				query: `
					mutation {
						getListStudents(
							student_input: {
								level: "${level}"
								class_name: "${class_name}"
							}
						) {
							student_id
							student_name
							student_surname
						}
					}              
				`
			})
			setStudents(r.data.data.getListStudents)
			history.push(teacherTaskPageLinkSelect(id))
			setLoading({ loading: false, fetched: true })
		} catch (e) {
			enqueueSnackbar(e.message, { variant: "error" })
			setLoading({ loading: false, fetched: false })
		}
	}

	const getStudentAnswers = async () => {
		if (!studentId) return
		if (studentLoading.loading) return
		setStudentLoading(v => ({ ...v, loading: true }))

		try {
			const r = await axios.post("", {
				query: `
					mutation {
						getSolveHomeWorkWithHomeWorkId(homeWork_input: {
							homeworkSolved_homework_id:"${id}",
							homeworkSolved_student_id: "${studentId}"
						}) {
							homeworkSolved_id
							homeworkSolved_homework_id,
							homeworkSolved_student_id,
							homeworkSolved_commentsTeacher,
							homeworkSolved_commentsStudent,
							homeworkSolved_file,
							homeworkSolved_build_date,
							homeworkSolved_update_date
						}
				    }              
				`
			})

			console.log(r.data)

			setAnswers(r.data.data.getSolveHomeWorkWithHomeWorkId)

			setStudentLoading({ loading: false, fetched: true })
		} catch (e) {
			enqueueSnackbar(e.message, { variant: "error" })
			setStudentLoading({ loading: false, fetched: false })
		}
	}

	return (
		<RootContainer
			loading={loading.loading}
			failed={!loading.fetched}
			onTryAgain={() => setFetchTrigger(v => v + 1)}>
			<div
				className={
					"flex items-stretch h-full relative w-full " +
					(loading.fetched ? " " : "invisible ")
				}
			>
				{/* main content */}
				<div className="flex-1 p-4 overflow-hidden">
					<ExerciseDetail
						editable
						studentId={studentId}
						task={homework}
						onEdit={(t) => {
							setHomework(t)
							getStudents(t.homeWork_level, t.homework_class_id)
						}}
					/>
					<Divider variant="middle" />
					{!!studentId ? (
						<div>
							{studentLoading.loading && <div className="flex items-center justify-center py-12 px-4">
								<CircularProgress color="secondary" size={48} />
							</div>}
							{!studentLoading.loading && !studentLoading.fetched && <div
								className="flex items-center justify-center py-12 px-4"
								onClick={getStudentAnswers}>
								<Button color="secondary" variant="contained">تلاش دوباره</Button>
							</div>}
							{!studentLoading.loading && studentLoading.fetched && <div>
								<ExerciseSolution answers={answers}
									onAnswersChange={a => setAnswers(a)}
									student={student()} />
							</div>}
						</div>
					) : <SelectStudetnBox />}
				</div>
				{/* main content */}
				{/* student list */}
				<div className="w-56 md:block hidden flex-shrink-0" />
				<div
					className=" w-56 md:block hidden overflow-y-auto fixed flex-shrink-0 bg-natural-light"
					style={{ left: 0, top: "64px", height: "calc(100vh - 64px)" }}
					dir="ltr"
				>
					{/* show student list in desktop */}
					<div className="w-full h-full " dir="rtl">
						<StudentList studentList={students} />
					</div>
					{/* end student list for desktop */}
					{/* show student list in desktop */}
					<Drawer
						className="fixed md:hidden w-64"
						anchor={"right"}
						open={drawerOpen}
						onClose={toggleDrawer}
					>
						<div className="w-64">
							<StudentList studentList={students} />
						</div>
					</Drawer>
					{/* end student list in desktop */}
				</div>
				{/* fab for show student list in mobile */}
				{/* student list */}
				<Fab
					onClick={toggleDrawer}
					className="fixed md:hidden bg-success text-white focus:outline-none z-50 left-0 bottom-0 me-4 mb-4"
					variant="extended"
				>
					<PeopleAltIcon className="me-1" />
					<span className="ms-1">دانش آموزان</span>
				</Fab>
				{/* fab for show student list in mobile */}
			</div>
		</RootContainer>
	);
};

export default TeacherTaskPage;
