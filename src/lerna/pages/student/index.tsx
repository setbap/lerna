import React, { useEffect, useState } from "react";
import Container from "@material-ui/core/Container";
import { ExerciseCard } from "../../shared/ExerciseCard";
import FilterListIcon from "@material-ui/icons/FilterList";
import SearchIcon from "@material-ui/icons/Search";
import IconButton from "@material-ui/core/IconButton";
import { studentTaskPageLinkSelect } from "../../shared/links";
import StudentFilterDialog from "../../components/student/filter/StudentFilterDialog";
import moment from "moment-jalaali";
import { useSnackbar } from 'notistack';
import { useAxios } from "../../contexts/AxiosContext";
import { Task } from "../../components/teacher/task/CreateTaskDialog";
import RootContainer from "../../components/RootContainer";
import { useSelector } from 'react-redux';
import { RootState } from "../../store";
import { fixHomeworks, toastResponseErrors } from "../../shared/axios";

const StudentPage = () => {

    const axios = useAxios()
    const { enqueueSnackbar } = useSnackbar();

    const student = useSelector((state: RootState) => state.student.current);
    const courseList = useSelector((state: RootState) => state.course.courses);

    const [homeworks, setHomeworks] = useState<Task[]>([])
    const [loading, setLoading] = useState<{ loading?: boolean, fetched?: boolean }>({ loading: false, fetched: false })
    const [openStudentFilterDialog, setOpenStudentFilterDialog] = useState(false);
    const [fetchTrigger, setFetchTrigger] = useState(0)

    const [filter, setFilter] = useState({
        date: moment().format("YYYY/MM/DD"),
        lessonId: null,
    })
    const [searchText, setSearchText] = useState("")

    const fetch = async () => {
        if (loading.loading) return
        setLoading(v => ({ ...v, loading: true }))

        try {
            const r = await axios.post("", {
                query: `
                    mutation {
                        getListHomeWorkWithDateForStudent (
                            get_list_homework: {
                                homework_start_date: "${filter.date}"
                                homework_class_id: "${student.class_name}"
                                homeWork_level: "${student.level}"
                            }
                        ) {
                            homework_id
                            homework_lesson_id
                            homework_class_id
                            homeWork_level
                            homework_teacher_id
                            homework_start_date
                            homework_end_date
                            homework_name
                            homework_description
                            homework_file
                            homework_build_date
                            homework_update_date
                        }
                    }              
					`
            })
            const hs = fixHomeworks(r.data.data.getListHomeWorkWithDateForStudent)
            if (hs === null) {
                toastResponseErrors(r, enqueueSnackbar)
                setLoading({ loading: false, fetched: false })
            } else {
                setHomeworks(hs)
                setLoading({ loading: false, fetched: true })
            }
        } catch (e) {
            enqueueSnackbar(e.message, { variant: "error" })
            setLoading({ loading: false, fetched: false })
        }
    }

    useEffect(() => {
        fetch()
        // eslint-disable-next-line
    }, [fetchTrigger])

    const filterHomeworks = () => {
        return homeworks
            ?.filter(h => !filter.lessonId || h.homework_lesson_id === filter.lessonId)
            ?.filter(h => !searchText || searchText === "" || h.homework_name.includes(searchText)) || []
    }

    return (
        <RootContainer
            loading={loading.loading}
            failed={!loading.fetched}
            onTryAgain={() => setFetchTrigger(v => v + 1)}>
            <Container maxWidth="lg" className="p-4">
                <div className="mb-6 justify-center items-center mt-2 mx-auto  max-w-xl flex">
                    <IconButton
                        className="me-3 ms-2 focus:outline-none"
                        aria-label="chnage filter"
                        onClick={() => setOpenStudentFilterDialog(true)}
                    >
                        <FilterListIcon />
                    </IconButton>
                    <StudentFilterDialog
                        open={openStudentFilterDialog}
                        onClose={() => setOpenStudentFilterDialog(false)}
                        filters={filter}
                        onChange={filters => {
                            console.log(filters.date, filter.date)
                            const needFetch = filters.date !== filter.date
                            setFilter(filters as any)
                            needFetch && setFetchTrigger(v => v + 1)
                        }} />
                    <label className=" border-grey-300 overflow-hidden bg-white  items-center border rounded-full p-1 flex-1 flex">
                        <input
                            className="flex-1 px-2 py-2 bg-transparent outline-none"
                            type="text"
                            value={searchText}
                            onChange={(v) => (setSearchText(v.target.value))}
                        />

                        <IconButton
                            className=" focus:outline-none"
                            aria-label="chnage filter"
                            size="small"
                        >
                            <SearchIcon className="m-1" />
                        </IconButton>
                    </label>
                </div>
                <div className="grid grid-cols-1  gap-6 md:grid-cols-2 ">
                    {filterHomeworks().map((homework, index) => (
                        <ExerciseCard
                            key={index}
                            startDate={homework.homework_start_date ? moment(homework.homework_start_date, "YYYY/MM/DD").toDate() : null}
                            endDate={homework.homework_end_date ? moment(homework.homework_end_date, "YYYY/MM/DD").toDate() : null}
                            serverTime={new Date("2020")}
                            nameOfClass={
                                "درس " +
                                (courseList?.find(c => c.id === homework?.homework_lesson_id)?.name || "نامشخص")
                            }
                            nameOfExercise={homework.homework_name}
                            selectedLink={studentTaskPageLinkSelect(homework.homework_id)}
                            viewer="student"
                        // anserwerd
                        />
                    ))}
                </div>
            </Container>
        </RootContainer >
    );
};

export default StudentPage;
