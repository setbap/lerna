import React, { FC, useEffect, useState } from "react";
import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";
import ExerciseDetail from "../../../shared/ExerciseDetail";
import Divider from "@material-ui/core/Divider";
import { ExerciseSolution } from "../../../shared/ExerciseSolution";
import StudentAnswerDialog from "../../../components/student/answer/StudentAnswerDialog";
import RootContainer from "../../../components/RootContainer";
import { useAxios } from "../../../contexts/AxiosContext";
import { useSnackbar } from "notistack";
import { RootState } from "../../../store";
import { useSelector } from 'react-redux';
import { Task } from "../../../components/teacher/task/CreateTaskDialog";
import { useParams } from "react-router-dom";
import { TaskAnswer } from "../../teacher/task";
import { fixHomework } from "../../../shared/axios";
type UrlType = {
    id: string;
};

const StudentTaskPage: FC = () => {
    let { id } = useParams<UrlType>();

    const [openStudentAnswerDialog, setOpenStudentAnswerDialog] = useState(false);

    const student = useSelector((state: RootState) => state.student.current);

    const axios = useAxios()
    const { enqueueSnackbar } = useSnackbar();

    const [loading, setLoading] = useState({ loading: false, fetched: false })
    const [homework, setHomework] = useState<Task>({})
    const [fetchTrigger, setFetchTrigger] = useState(0)
    const [answers, setAnswers] = useState<TaskAnswer[]>([])

    useEffect(() => {
        fetch()
        // eslint-disable-next-line
    }, [fetchTrigger])

    const fetch = async () => {
        if (loading.loading) return
        setLoading(v => ({ ...v, loading: true }))

        try {
            const r = await axios.post("", {
                query: `
                    mutation {
                        getHomeWorkInfo(get_list_homework: { homework_id: "${id}" }) {
							homework_id
							homework_lesson_id
							homework_class_id
							homeWork_level
							homework_teacher_id
							homework_start_date
							homework_end_date
							homework_name
							homework_description
							homework_file
							homework_build_date
							homework_update_date
						}
                        getSolveHomeWorkWithHomeWorkId(homeWork_input: {
							homeworkSolved_homework_id:"${id}",
							homeworkSolved_student_id: "${student.student_id}"
						}) {
							homeworkSolved_id
							homeworkSolved_homework_id,
							homeworkSolved_student_id,
							homeworkSolved_commentsTeacher,
							homeworkSolved_commentsStudent,
							homeworkSolved_file,
							homeworkSolved_build_date,
							homeworkSolved_update_date
                        }
                    }          
				`
            })

            console.log(r.data)

            let h = r.data.data.getHomeWorkInfo
            if (h.length === 0) {
                // eslint-disable-next-line
                throw { code: 404, message: "تکلیف پیدا نشد" }
            } else {
                h = h[0]
            }
            setHomework(fixHomework(h))

            setAnswers(r.data.data.getSolveHomeWorkWithHomeWorkId)

            setLoading({ loading: false, fetched: true })
        } catch (e) {
            enqueueSnackbar(e.message, { variant: "error" })
            setLoading({ loading: false, fetched: false })
        }
    }

    return (
        <RootContainer
            loading={loading.loading}
            failed={!loading.fetched}
            onTryAgain={() => setFetchTrigger(v => v + 1)}>
            <div className={
                "flex items-stretch md:p-2  h-full relative w-full " +
                (loading.fetched ? " " : "invisible ")
            }>
                {/* main content */}
                <div className="flex-1 p-4 overflow-hidden">
                    <ExerciseDetail task={homework} editable={false} />
                    <Divider variant="middle" />
                    {/* <SelectStudetnBox /> */}
                    <ExerciseSolution isStudent answers={answers} onAnswersChange={a => setAnswers(a)} student={student} />
                </div>
                {/* main content */}
                {/* fab for show student list in mobile */}
                <Fab
                    onClick={() => setOpenStudentAnswerDialog(true)}
                    className="fixed  bg-success text-white focus:outline-none z-50 left-0 bottom-0 me-4 mb-4"
                    variant="extended"
                >
                    <AddIcon className="me-1" />
                    <span className="ms-1">افزودن تصویر</span>
                </Fab>
                {/* fab for show student list in mobile */}
                <StudentAnswerDialog
                    open={openStudentAnswerDialog}
                    onClose={() => setOpenStudentAnswerDialog(false)}
                    answer={{
                        homeworkSolved_homework_id: id,
                        homeworkSolved_student_id: student.student_id,
                    }}
                    onDone={(answer) => {
                        setAnswers(v => [...v, answer])
                    }}
                />
            </div>
        </RootContainer>
    );
};

export default StudentTaskPage;
