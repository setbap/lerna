import React from "react";
import { Switch, Route } from "react-router-dom";
import DialogsPage from "./pages/dialogs";
import MainIndex from "./pages/index/index";
import StudentPage from "./pages/student";
import TeacherPage from "./pages/teacher";
import TeacherTaskPage from "./pages/teacher/task";
import StudentTaskPage from "./pages/student/task";
import { store } from "./store";
import { Provider } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import { BrowserRouter as Router } from "react-router-dom";
import { create } from "jss";
import { SnackbarProvider } from 'notistack';
import { AxiosProvider } from "./contexts/AxiosContext";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import rtl from "jss-rtl";
import { StylesProvider, jssPreset } from "@material-ui/core/styles";
import cyan from "@material-ui/core/colors/cyan";
import "./index.css";

import {
    MainPageLink,
    StudentPageLink,
    TeacherPageLink,
    TeacherTaskPageLink,
    DialogsLink,
    StudentTaskPageLink,
    TeacherTaskPageWithStudentLink,
} from "./shared/links";

document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");

const theme = createMuiTheme({
    direction: "rtl",
    typography: {
        fontFamily: [
            "Samim",
            "-apple-system",
            "BlinkMacSystemFont",
            '"Segoe UI"',
            "Roboto",
            '"Helvetica Neue"',
            "Arial",
            "sans-serif",
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(","),
    },
    palette: {
        primary: cyan,
    },
    overrides: {
        MuiStepIcon: {
            text: {
                fill: "white",
            },
            root: {
                color: "#C3D1E0",
            },
        },
        MuiInputBase: {
            root: {
                background: "#ECF1F2 !important",
            },
        },
    },
});

const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

export const LernaRoutes = () => {
    return (
        <>
            <Route exact path={MainPageLink}>
                <MainIndex />
            </Route>
            <Route exact path={TeacherPageLink}>
                <TeacherPage />
            </Route>
            <Route exact path={DialogsLink}>
                <DialogsPage />
            </Route>
            <Route path={[TeacherTaskPageWithStudentLink, TeacherTaskPageLink]}>
                <TeacherTaskPage />
            </Route>
            <Route path={StudentTaskPageLink}>
                <StudentTaskPage />
            </Route>
            <Route path={StudentPageLink}>
                <StudentPage />
            </Route>
        </>
    )
};

const LernaApp = () => {
    return (
        <React.StrictMode>
            <Router>
                <CssBaseline />
                <MuiThemeProvider theme={theme}>
                    <StylesProvider jss={jss}>
                        <Provider store={store}>
                            <SnackbarProvider maxSnack={3}>
                                <AxiosProvider>
                                    <Switch>
                                        <LernaRoutes />
                                    </Switch>
                                </AxiosProvider>
                            </SnackbarProvider>
                        </Provider>
                    </StylesProvider>
                </MuiThemeProvider>
            </Router>
        </React.StrictMode>
    );
}

export default LernaApp;
