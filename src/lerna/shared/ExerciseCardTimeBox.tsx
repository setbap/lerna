import React from "react";
import TimerIcon from "@material-ui/icons/Timer";
import { Typography } from "@material-ui/core";
interface IExerciseCardTimeBox {
  time: string;
  type: "startDate" | "endDate";
  colorClass?: string;
}

const ExerciseCardTimeBox = ({
  time,
  colorClass,
  type = "startDate",
}: IExerciseCardTimeBox) => {
  const typeName = type === "startDate" ? "تاریخ شروع" : "تاریخ پایان";
  return (
    <span>
      <div className="flex flex-col  justify-start items-center">
        <div className="flex justify-around px-1 text-center items-center ">
          <TimerIcon className={`px-1 ${colorClass}`} />
          <div className=" mx-1 md:mx-2">{typeName}</div>
        </div>
        <Typography variant="caption" align="center" className="" gutterBottom>
          {time}
        </Typography>
      </div>
    </span>
  );
};

export { ExerciseCardTimeBox };
