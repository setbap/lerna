import React from "react";
import EditIcon from "@material-ui/icons/Edit";
import jMoment from "moment-jalaali";
import IconButton from "@material-ui/core/IconButton/IconButton";
import { ExerciseCardTimeBox } from "./ExerciseCardTimeBox";
import DoneAllIcon from "@material-ui/icons/DoneAll";
import { Link } from "react-router-dom";

interface IExerciseCard {
  viewer: "teacher" | "student";
  nameOfExercise: string;
  nameOfClass: string;
  id?: string;
  onClickEditButton?: VoidFunction;
  startDate: Date;
  endDate: Date;
  anserwerd?: boolean;
  serverTime: Date;
  selectedLink: string;
}

const ExerciseCard = ({
  nameOfExercise,
  nameOfClass,
  viewer,
  endDate,
  selectedLink,
  startDate,
  serverTime,
  onClickEditButton,
  anserwerd,
  id,
}: IExerciseCard) => {
  const serverTimeMoment = jMoment(serverTime);
  const startDateMoment = jMoment(startDate);
  const endDateMoment = jMoment(endDate);
  const momentFormatType = "jYYYY/jMM/jDD";

  const statusThemeColorClass: () => {
    border: string;
    textColor: string;
  } = () => {
    if (serverTimeMoment < startDateMoment) {
      return { border: "border-success", textColor: "text-success" };
    }
    if (serverTimeMoment > endDateMoment) {
      return { border: "border-error", textColor: "text-error" };
    }
    return { border: "border-warning", textColor: "text-warning" };
  };

  const statusThemeColor = statusThemeColorClass();

  return (
    <div
      className={`${statusThemeColor.border} relative md:p-4 p-2 border-2 shadow-sm items-start rounded-xl transition-shadow duration-300 bg-white hover:shadow-xl`}
    >
      {anserwerd && viewer === "student" && (
        <div className="absolute flex justify-center items-center left-0 top-0 transform -translate-x-px -translate-y-px w-10 rounded-tl-lg rounded-br-xl bg-success h-10">
          <DoneAllIcon className="text-white" />
        </div>
      )}
      {/* name and edit icon div */}
      <div className="flex mb-6 sm:mb-12 justify-between items-start">
        <div className="px-2 font-bold">
          <Link to={selectedLink}>
            <div className="mb-2 underline">{nameOfExercise}</div>
          </Link>
          <div className="font-light text-xs text-gray-700">{nameOfClass}</div>
        </div>
        {viewer === "teacher" && (
          <div className="absolute m-1 left-0 top-0  ">
            <IconButton
              aria-label="edit"
              size="medium"
              {...{ onClick: onClickEditButton }}
              className="  origin-center focus:outline-none"
            >
              <EditIcon className="outline-none " />
            </IconButton>
          </div>
        )}
      </div>
      {/* end name and edit icon div */}

      {/* start and end date  div */}
      <div className="grid grid-cols-2 text-start  ">
        <div className="col-span-2">
          <ExerciseCardTimeBox
            type="startDate"
            colorClass={statusThemeColor.textColor}
            time={startDateMoment.format(momentFormatType)}
          />
        </div>
        {/* <div className="">
          <ExerciseCardTimeBox
            type="endDate"
            colorClass={statusThemeColor.textColor}
            time={endDateMoment.format(momentFormatType)}
          />
        </div> */}
      </div>
      {/* start and end date  div */}
    </div>
  );
};

export { ExerciseCard };
