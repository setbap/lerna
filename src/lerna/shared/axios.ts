import axios, { AxiosError, AxiosResponse } from "axios";
import { Task } from "../components/teacher/task/CreateTaskDialog";

// export const BaseUrl = "http://localhost:4000";
export const BaseUrl = "https://moored-fantastic-turret.glitch.me";

// export const UploadUrl = "http://localhost:8080";
export const UploadUrl = "https://raymon-tech.ir";

export const FileUploadedUrl =
  "https://kamal-chat.s3.ir-thr-at1.arvanstorage.com/";

axios.defaults.baseURL = BaseUrl + "/graphql";

export const toastErrors = (e: Error, enqueueSnackbar: Function) => {
  let message = e.message;

  const ae: AxiosError = e as any;
  const errors = ae.response?.data?.errors as any[];

  if (errors && errors.length > 0) {
    message = errors[0].message;
  }

  if (!message || message === "") message = "خطای غیر منتظره";

  enqueueSnackbar(message, { variant: "error" });
};

export const toastResponseErrors = (
  r: AxiosResponse,
  enqueueSnackbar: Function
) => {
  let message = r.statusText;

  const errors = r.data?.errors as any[];

  if (errors && errors.length > 0) {
    message = errors[0].message;
  }

  if (!message || message === "") message = "خطای غیر منتظره";

  enqueueSnackbar(message, { variant: "error" });
};

export const fixHomeworks = (homeworks: any[]): Task[] => {
  return homeworks
    ?.map((h) => {
      return fixHomework(h);
    })
    .filter((h) => h !== null);
};

export const fixHomework = (homework: any): Task | null | undefined => {
  console.log(homework);
  if (homework?.homework_file !== undefined)
    homework.homework_file =
      homework?.homework_file?.split(",")?.filter((s: any) => s !== "") || [];
  console.log(homework);

  return homework;
};

const initAxios = () => {
  return axios;
};

export default initAxios;
