import React from "react";
import MessageIcon from "@material-ui/icons/Message";
import Divider from "@material-ui/core/Divider";
import { Typography } from "@material-ui/core";
interface IAnswerItem {
  count: number;
  teacherNote?: string;
  studentNote?: string;
  imgSrc: string;
  isReviewed?: boolean;
  onImageClick?: () => void;
}

const AnswerItem = ({
  count,
  imgSrc,
  isReviewed = false,
  studentNote,
  teacherNote,
  onImageClick,
}: IAnswerItem) => {

  const validUrl = (url?: string) => {
    return url && (url.toLocaleLowerCase().startsWith("http://") || url.toLocaleLowerCase().startsWith("https://"))
  }
  return (
    <div className="flex flex-col  md:flex-row flex-no-wrap max-w-screen-xl md:items-start m-auto   justify-center sm:justify-center  mt-12">
      <div className="relative w-10/12 h-40 md:h-auto self-center md:self-start  md:w-32  transform md:-translate-x-12 md:translate-y-16 md:scale-200    ">
        <div className="  w-1/1 start-0 pt-1/1   overflow-hidden">
          <img
            onClick={onImageClick}
            alt=""
            src={validUrl(imgSrc) ? imgSrc : "/lerna/img/placeholder.png"}
            className={"absolute start-0 top-0 w-full h-full  object-cover w shadow-lg rounded-xl " + (onImageClick ? "cursor-pointer hover:scale-105 transform transition-transform duration-300 " : " ")}
          />
        </div>
      </div>

      <div className="flex bg-white flex-col border justify-start  w-1/1 md:mt-12 -mt-16 pt-20 md:pt-6 px-6 md:px-0 md:ps-28 lg:ps-32  md:self-end rounded-xl  py-10 ">
        <div className="flex  items-center">
          <MessageIcon className="text-success mt-1" />
          <Typography className="md:mx-4 mx-2 text-sm md:text-base font-bold">
            نظر داشنجو
          </Typography>
        </div>
        <Typography className="leading-relaxed mt-1 pe-8 text-xs md:text-sm ">
          {studentNote ? studentNote : "نظزی توسط دانش آموز ثبت نشده"}
        </Typography>
        <Divider className="my-5 w-11/12" />
        <div className="flex items-center">
          <MessageIcon className="text-warning mt-1" />
          <Typography className="md:mx-4 mx-2 text-sm md:text-base font-bold">
            نظر معلم
          </Typography>
        </div>
        <Typography className="leading-7 mt-1 pe-8  text-justify text-xs md:text-sm ">
          {teacherNote ? teacherNote : "نظزی توسط معلم ثبت نشده"}
        </Typography>
      </div>
    </div>
  );
};

export { AnswerItem };
