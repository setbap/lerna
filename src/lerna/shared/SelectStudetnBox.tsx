import React from "react";
import ErrorIcon from "@material-ui/icons/Error";
import { Typography } from "@material-ui/core";
interface ISelectStudetnBox {}

const SelectStudetnBox = (_: ISelectStudetnBox) => {
  return (
    <div className="border rounded-3xl mt-8 bg-white border-warning px-6 py-8 text-center ">
      <ErrorIcon
        style={{ fontSize: 64 }}
        className=" text-warning mx-auto mb-6"
      />
      <Typography variant={"h6"}>
        یک دانش آموز از لیست دانش آموزان انتخاب نمایید تا پاسخ های آن را مشاهده
        نمایید
      </Typography>
    </div>
  );
};

export { SelectStudetnBox };
