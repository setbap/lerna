import { UploadUrl, FileUploadedUrl } from "./axios";
import axios from "axios";
import SparkMD5 from "spark-md5";
import { v4 as uuidv4 } from "uuid";

export const uploadFile = async (
  globalfile: File,
  onUploadProgress?: (ProgressEvent) => void
) => {
  var fileIdL = globalfile.name.split(".");
  const format = fileIdL[fileIdL.length - 1].toLowerCase();
  const chunkSize = 8 * 1024 * 1024; // The size of each chunk, set to 1 Megabyte
  var blobSlice =
    File.prototype.slice ||
    // @ts-ignore
    File.prototype.mozSlice ||
    // @ts-ignore
    File.prototype.webkitSlice;
  ///////////////
  var mimetype = "";
  ///////////////////////////////
  switch (format) {
    case "gif":
      // Graphics Interchange Format
      mimetype = "image/gif";
      break;
    case "mp3":
      // Graphics Interchange Format
      mimetype = "video/mp4";
      break;
    case "mp4":
      // Graphics Interchange Format
      mimetype = "video/mp4";
      break;
    case "jpm":
      // JPEG 2000 Compound Image File Format
      mimetype = "video/jpm";
      break;
    case "jpeg":
      // JPEG Image
      mimetype = "image/jpeg";
      break;
    case "jpg":
      // JPEG Image
      mimetype = "image/jpg";
      break;
    case "jpgv":
      // JPGVideo
      mimetype = "video/jpeg";
      break;
    case "png":
      // Portable Network Graphics (PNG)
      mimetype = "image/png";
      break;
    case "ppm":
      // Portable Pixmap Format
      mimetype = "image/x-portable-pixmap";
      break;
    case "svg":
      // Scalable Vector Graphics (SVG)
      mimetype = "image/svg+xml";
      break;
    case "tiff":
      // Tagged Image File Format
      mimetype = "image/tiff";
      break;
    case "xbm":
      // X BitMap
      mimetype = "image/x-xbitmap";
      break;
    case "xpm":
      // X PixMap
      mimetype = "image/x-xpixmap";
      break;
    case "xwd":
      // X Window Dump
      mimetype = "image/x-xwindowdump";
      break;
    case "mdb":
      // Microsoft Access
      mimetype = "application/x-msaccess";
      break;
    case "xls":
      // Microsoft Excel
      mimetype = "application/vnd.ms-excel";
      break;
    case "xlam":
      // Microsoft Excel - Add-In File
      mimetype = "application/vnd.ms-excel.addin.macroenabled.12";
      break;
    case "xlsb":
      // Microsoft Excel - Binary Workbook
      mimetype = "application/vnd.ms-excel.sheet.binary.macroenabled.12";
      break;
    case "xltm":
      // Microsoft Excel - Macro-Enabled Template File
      mimetype = "application/vnd.ms-excel.template.macroenabled.12";
      break;
    case "xlsm":
      // Microsoft Excel - Macro-Enabled Workbook
      mimetype = "application/vnd.ms-excel.sheet.macroenabled.12";
      break;
    case "pptx":
      // Microsoft Office - OOXML - Presentation
      mimetype =
        "application/vnd.openxmlformats-officedocument.presentationml.presentation";
      break;
    case "sldx":
      // Microsoft Office - OOXML - Presentation (Slide)
      mimetype =
        "application/vnd.openxmlformats-officedocument.presentationml.slide";
      break;
    case "ppsx":
      // Microsoft Office - OOXML - Presentation (Slideshow)
      mimetype =
        "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
      break;
    case "potx":
      // Microsoft Office - OOXML - Presentation Template
      mimetype =
        "application/vnd.openxmlformats-officedocument.presentationml.template";
      break;
    case "xlsx":
      // Microsoft Office - OOXML - Spreadsheet
      mimetype =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
      break;
    case "xltx":
      // Microsoft Office - OOXML - Spreadsheet Teplate
      mimetype =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
      break;
    case "docx":
      // Microsoft Office - OOXML - Word Document
      mimetype =
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
      break;
    case "dotx":
      // Microsoft Office - OOXML - Word Document Template
      mimetype =
        "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
      break;
    case "ppt":
      // Microsoft PowerPoint
      mimetype = "application/vnd.ms-powerpoint";
      break;
    case "ppam":
      // Microsoft PowerPoint - Add-in file
      mimetype = "application/vnd.ms-powerpoint.addin.macroenabled.12";
      break;
    case "sldm":
      // Microsoft PowerPoint - Macro-Enabled Open XML Slide
      mimetype = "application/vnd.ms-powerpoint.slide.macroenabled.12";
      break;
    case "pptm":
      // Microsoft PowerPoint - Macro-Enabled Presentation File
      mimetype = "application/vnd.ms-powerpoint.presentation.macroenabled.12";
      break;
    case "ppsm":
      // Microsoft PowerPoint - Macro-Enabled Slide Show File
      mimetype = "application/vnd.ms-powerpoint.slideshow.macroenabled.12";
      break;
    case "mpp":
      // Microsoft Project
      mimetype = "application/vnd.ms-project";
      break;
    case "doc":
      // Microsoft Word
      mimetype = "application/msword";
      break;
    case "wri":
      // Microsoft Wordpad
      mimetype = "application/x-mswrite";
      break;
    case "wps":
      // Microsoft Works
      mimetype = "application/vnd.ms-works";
      break;
    case "pdf":
      // Adobe Portable Document Format
      mimetype = "application/pdf";
      break;
    case "txt":
      // Text File
      mimetype = "text/plain";
      break;
    default:
      break;
  }

  var file = new File([globalfile], uuidv4() + `.${format}`, {
    type: mimetype,
  });
  //---
  if (!file) {
    return Promise.reject({ message: "No file was obtained" });
  }
  ////////////////////////
  var maxUploadTries = 5;
  const sendToServerFilePart = async (data, axiosOptions, maxUploadTries) => {
    try {
      return await axios.post(UploadUrl + "/file/upload", data, axiosOptions);
    } catch {
      if (0 < maxUploadTries) {
        return await sendToServerFilePart(
          data,
          axiosOptions,
          maxUploadTries - 1
        );
      } else {
        return Promise.reject({ message: "پس از مدتی مجددا امتحان نمایید!!!" });
      }
    }
  };

  const blockCount = Math.ceil(file.size / chunkSize); // Total number of slices
  const hash = await hashFile(file, chunkSize, blobSlice); //File hash
  for (let i = 0; i < blockCount; i++) {
    const start = i * chunkSize;
    const end = Math.min(file.size, start + chunkSize);
    var fileSlice = blobSlice.call(file, start, end);
    const axiosOptions = {
      timeout: 1800000,
      onUploadProgress: (e) => {
        onUploadProgress && onUploadProgress(e);
        const progress = (e.loaded / e.total / blockCount) * 100;
        console.log(blockCount, i, progress, file);
      },
    };
    const form = new FormData();
    form.append("file", fileSlice);
    form.append("name", file.name);
    form.append("total", blockCount as any);
    form.append("index", i as any);
    form.append("size", file.size as any);
    form.append("hash", hash as any);
    try {
      await sendToServerFilePart(form, axiosOptions, maxUploadTries);
    } catch (e) {
      return Promise.reject(e);
    }
  }
  const data = {
    size: file.size,
    name: file.name,
    total: blockCount,
    hash,
  };
  return await axios
    .post(UploadUrl + "/file/merge_chunks", data)
    .then((res) => {
      if (res.data && res.data.seccess) {
        return Promise.resolve({
          name: file.name,
          url: FileUploadedUrl + file.name,
          ...res.data,
        });
      } else {
        return Promise.reject({
          message: "خطایی رخ داده مجددا ارسال نمایید",
        });
      }
    })
    .catch((e) => {
      return Promise.reject(e);
    });
};

function hashFile(file, chunkSize, blobSlice) {
  return new Promise((resolve, reject) => {
    const chunks = Math.ceil(file.size / chunkSize);
    let currentChunk = 0;
    const spark = new SparkMD5.ArrayBuffer();
    const fileReader = new FileReader();
    function loadNext() {
      const start = currentChunk * chunkSize;
      const end =
        start + chunkSize >= file.size ? file.size : start + chunkSize;
      fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
    }
    fileReader.onload = (e) => {
      spark.append(e.target.result); // Append array buffer
      currentChunk += 1;
      if (currentChunk < chunks) {
        loadNext();
      } else {
        console.log("finished loading");
        const result = spark.end();
        // If result s are used as hash values only, if the contents of the file are the same and the names are different
        // You cannot keep two files if you want to.So add the file name.
        const sparkMd5 = new SparkMD5();
        sparkMd5.append(result);
        sparkMd5.append(file.name);
        const hexHash = sparkMd5.end();
        resolve(hexHash);
      }
    };
    fileReader.onerror = () => {
      reject("File reading failed!");
    };
    loadNext();
  }).catch((err) => {
    console.log(err);
  });
}
