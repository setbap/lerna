import { BaseUrl } from "../config";

export const TeacherPageLink = BaseUrl + "/teacher";
export const TeacherTaskPageLink = BaseUrl + "/teacher/task/:id";
export const TeacherTaskPageWithStudentLink =
  BaseUrl + "/teacher/task/:id/:studentId";
export const StudentTaskPageLink = BaseUrl + "/student/task/:id";
export const DialogsLink = BaseUrl + "/dialogs";
export const StudentPageLink = BaseUrl + "/student";
export const MainPageLink = BaseUrl + "/";

export const teacherTaskPageLinkSelect = (id: string) =>
  BaseUrl + `/teacher/task/${id}`;
export const teacherTaskPageWithStudentLinkSelect = ({
  id,
  studentId,
}: {
  id: string;
  studentId: string;
}) => BaseUrl + `/teacher/task/${id}/${studentId}`;

// export const teacherTaskPageGoToStudentPage = ({
//   studentId,
// }: {
//   studentId: string;
// }) => BaseUrl + `/${studentId}`;

export const studentTaskPageLinkSelect = (id: string) =>
  BaseUrl + `/student/task/${id}`;
