import { Button, Typography } from "@material-ui/core";
import Chip from "@material-ui/core/Chip/Chip";
import EditIcon from "@material-ui/icons/Edit";
import React, { FC, useState } from "react";
import { ExerciseCardTimeBox } from "./ExerciseCardTimeBox";
import FileList from "../components/utils/FIleList";

import jMoment from "moment-jalaali";
import CreateTaskDialog, { Task } from "../components/teacher/task/CreateTaskDialog";
import { useSelector } from "react-redux";
import { RootState } from "../store";

type ExerciseDetailProps = {
    editable?: boolean
    task?: Task,
    studentId?: string
    onEdit?: (task: Task) => void
}

const ExerciseDetail: FC<ExerciseDetailProps> = ({ editable, task, onEdit }) => {

    const courseList = useSelector((state: RootState) => state.course.courses);
    const levelList = useSelector((state: RootState) => state.level.levels);
    const classList = useSelector((state: RootState) => state.class.classes);

    const momentFormatType = "jYYYY/jMM/jDD";

    const now = jMoment("YYYY/MM/DD");
    const startDateMoment = jMoment(task.homework_start_date, "YYYY/MM/DD");
    const endDateMoment = jMoment(task.homework_end_date, "YYYY/MM/DD");

    const [openEditTaskDialog, setOpenEditTaskDialog] = useState(false);

    const ended = () => {
        return now.isAfter(endDateMoment)
    }

    return (
        <div className="w-full">
            <div className="flex justify-between items-center w-full">
                <div className="flex-1">
                    <div className="flex items-center">
                        <Typography variant={"h5"} className="text-grey-900">
                            {task.homework_name}
                        </Typography>
                        {ended() && <Chip
                            variant="outlined"
                            // color="secondary"
                            className={
                                "text-error border-error transform scale-75 md:scale-100 md:ms-3"
                            }
                            label={"به پایان رسیده"}
                            size="small"
                        />}
                    </div>
                    <Typography variant={"subtitle2"} className="text-grey-700 mt-2 ">
                        {editable ?
                            (
                                "کلاس " +
                                (levelList?.find(c => c.id === task?.homeWork_level)?.name || "نامشخص") +
                                " " +
                                (classList?.find(c => c.id === task?.homework_class_id)?.name || "")
                                + " - " +
                                "درس " +
                                (courseList?.find(c => c.id === task?.homework_lesson_id)?.name || "نامشخص")
                            )
                            : (
                                "درس " +
                                (courseList?.find(c => c.id === task?.homework_lesson_id)?.name || "نامشخص")
                            )
                        }
                    </Typography>
                </div>
                <div className=" transform scale-75 md:scale-100 -me-4 md:me-0">
                    {editable && (
                        <Button
                            className="rounded-2xl bg-grey-300 focus:outline-none"
                            onClick={() => setOpenEditTaskDialog(true)}
                            startIcon={<EditIcon className=" ms-2" />}
                        >
                            <div>ویرایش</div>
                        </Button>
                    )}
                </div>
            </div>
            {/* start and end date  div */}
            <div className="grid md:grid-cols-3 mt-8 grid-cols-2 text-start  ">
                <div className="col-span-2">
                    <ExerciseCardTimeBox
                        type="startDate"
                        colorClass={"text-black"}
                        time={startDateMoment.format(momentFormatType)}
                    />
                </div>
                {/* <div className="">
                    <ExerciseCardTimeBox
                        type="endDate"
                        colorClass={"text-black"}
                        time={endDateMoment.format(momentFormatType)}
                    />
                </div> */}
            </div>
            {/* and end date  div */}

            {/* Attachment file */}
            <div className="flex flex-col md:flex-row items-stretch  justify-center md:justify-start  text-center  w-full mt-8">
                <div className="self-center font-bold text-lg text-grey-700 flex-shrink-0">
                    ضمیمه ها
				</div>
                <div className=" h-px w-full md:w-px border md:h-auto self-stretch rounded-full bg-grey-400 border-grey-400 mx-0 my-2 md:my-0 md:mx-2 flex-shrink-0 " />
                <div className="flex-1 overflow-hidden">
                    <FileList
                        className=""
                        files={task.homework_file}
                        onDownload={(file) => console.log(file)}
                        removable={false}
                        size="small"
                    />
                </div>
            </div>
            {/* Attachment file */}

            <div className="flex flex-col md:flex-row items-stretch  justify-center md:justify-start  text-center  w-full mt-10">
                <div className="self-center font-bold text-lg text-grey-700 flex-shrink-0">
                    توضیحات
				</div>
                <div className=" h-px w-full md:w-px border md:h-auto self-stretch rounded-full bg-grey-400 border-grey-400 mx-0 my-2 md:my-0 md:mx-2 flex-shrink-0 " />
                <Typography
                    className="text-sm md:text-base flex-1 leading-7 md:leading-8"
                    align="justify"
                >
                    {task.homework_description}
                </Typography>
            </div>
            <div className="mb-10" />
            <CreateTaskDialog
                open={openEditTaskDialog}
                isEdit
                onClose={() => setOpenEditTaskDialog(false)}
                infoData={task}
                onDone={(t) => {
                    if (!t) return
                    onEdit && onEdit(t)
                }}
                files={task.homework_file} />
        </div>
    );
};

export default ExerciseDetail;
