import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import { Typography } from "@material-ui/core";
import { AnswerItem } from "./AnswerItem";
import TeacherAnswerDialog from "../components/teacher/answer/TeacherAnswerDialog";
import { Student } from "../store/studentSlice";
import { TaskAnswer } from "../pages/teacher/task";

interface IExerciseSolution {
    isStudent?: boolean,
    student?: Student | null,
    answers?: TaskAnswer[] | null
    onAnswersChange?: (answers: TaskAnswer[] | null) => void
}

const ExerciseSolution = (props: IExerciseSolution) => {
    const { isStudent, student, answers, onAnswersChange } = props
    const [openTeacherAnswerDialog, setOpenTeacherAnswerDialog] = useState(false);
    const [targetAnswer, setTargetAnswer] = useState<TaskAnswer | null>(null);

    const checked = () => {
        return (answers?.filter(a => !a.homeworkSolved_commentsTeacher || a.homeworkSolved_commentsTeacher === "")?.length || 0) === 0
    }
    return (
        <div>
            {/* solution header  */}
            <div className="mt-8 relative rounded-full p-1 z-10 flex items-center justify-between bg-natural-light">
                <Avatar
                    className="lg:w-16 w-12 h-12 lg:h-16"
                    src={"https://www.w3schools.com/howto/img_avatar.png"}
                />
                <Typography
                    className="text-start flex-1 ms-4 font-bold"
                    variant="subtitle1"
                >
                    {student?.student_name + " " + student?.student_surname}
                </Typography>

                {!!answers?.length && (<div className={
                    "my-2 ms-2 me-3 lg:me-5 py-1 px-2  rounded-full text-white " +
                    (checked() ? "bg-green-500" : "bg-red-500")
                }>
                    {checked() ? "بررسی شده" : "بررسی نشده"}
                </div>)}
                <div className="absolute bottom-0 transform translate-y-4 -translate-x-20 z-auto rotate-45 start-0 bg-natural-light w-8 h-8" />
            </div>
            {/* solution header  */}
            <div>
                {answers?.map((answer, index) => (
                    <AnswerItem
                        key={index + 1}
                        count={index + 1}
                        imgSrc={answer.homeworkSolved_file}
                        isReviewed={answer.homeworkSolved_commentsTeacher && answer.homeworkSolved_commentsTeacher !== ""}
                        studentNote={answer.homeworkSolved_commentsStudent}
                        teacherNote={answer.homeworkSolved_commentsTeacher}
                        onImageClick={!isStudent && (() => {
                            setTargetAnswer(answer)
                            setOpenTeacherAnswerDialog(true)
                        })}
                    />
                ))}
            </div>
            <TeacherAnswerDialog
                open={openTeacherAnswerDialog}
                onClose={() => setOpenTeacherAnswerDialog(false)}
                answer={targetAnswer}
                onDone={na => onAnswersChange(answers.map(a => a.homeworkSolved_id === na?.homeworkSolved_id ? na : a))} />
        </div>
    );
};

export { ExerciseSolution };
