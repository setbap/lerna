import React, { FC } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import PersonIcon from "@material-ui/icons/Person";
import { useHistory, useParams } from "react-router-dom";
import {
  teacherTaskPageLinkSelect,
  teacherTaskPageWithStudentLinkSelect,
} from "./links";
import { Student } from "../store/studentSlice";

type StudentListProps = {
  studentList: Student[]
}

const StudentList: FC<StudentListProps> = ({ studentList }) => {
  let { id, studentId } = useParams<{ id: string; studentId?: string }>();
  const history = useHistory();

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={"bg-natural-light "}
    >
      <ListItem
        button
        onClick={() => history.push(teacherTaskPageLinkSelect(id))}
      >
        <ListItemText primary="لیست دانش آموزان" />
      </ListItem>
      {studentList.map((student, index) => (
        <ListItem
          onClick={() =>
            history.push(
              teacherTaskPageWithStudentLinkSelect({
                id: id,
                studentId: student.student_id,
              })
            )
          }
          className={`
          transition-all duration-300
          ${!!studentId && studentId === student.student_id + "" ? "text-white bg-success" : ""
            }`}
          key={index}
          button
        >
          <Avatar>
            <PersonIcon />
          </Avatar>
          <ListItemText className="px-2 font-light whitespace-no-wrap  overflow-hidden" primary={student.student_name + " " + student.student_surname} />
        </ListItem>
      ))}
    </List>
  );
};

export { StudentList };
