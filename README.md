# Lerna

a react project

## add to another react project

root of lerna project = `[lerna]`
root of your project = `[you]`

**1.** install dependencies

```shell=
   yarn add tailwindcss@npm:@tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9 @craco/craco @date-io/dayjs@1.x @date-io/jalaali@1.x @material-ui/core @material-ui/icons @material-ui/lab @material-ui/pickers@3.2.10 @reduxjs/toolkit @types/node @types/react @types/react-dom @types/react-redux axios jss-rtl @types/jest moment-jalaali notistack react-indiana-drag-scroll react-redux react-router-dom react-zoom-pan-pinch spark-md5 tailwindcss-ripple typescript@4.0.5 use-state-with-callback uuid tailwindcss-rtl
```

**2.** add font to html head in `[you]/public/index.html`

```html=
    <link href="https://cdn.jsdelivr.net/gh/rastikerdar/samim-font@v4.0.5/dist/font-face.css" rel="stylesheet" type="text/css" />
```

**3.** update your `scripts` in your `package.json` file to use `craco` instead of `react-scripts` for all `scripts` except `eject`

```json=
{
    // ...
   "scripts": {
      "start": "craco start",
      "build": "craco build",
      "test": "craco test",
      "eject": "react-scripts eject"
   },
}
```

**4.** create a `craco.config.js` at the root of your project and add the `tailwindcss` and `autoprefixer` as PostCSS plugins

```js=
// craco.config.js
module.exports = {
  style: {
    postcss: {
      plugins: [
         require('tailwindcss'),
         require('autoprefixer'),
      ],
    },
  },
}
```

**5.** generate your `tailwind.config.js` file:

```shell=
npx tailwindcss init
```

**6.** move `[lerna]/tailwind.config.js` to `[you]/tailwind.config.js`

**7.** move `[lerna]/tsconfig.json` to `[you]/tsconfig.json` and if you already have a tsconfig.json or jsconfig.json file in your project root combine this two in tsconfig.json.

**8.** move all files inside folder `[lerna]/src/lerna` to folder `[you]/src/lerna`

**9.** move all files inside folder `[lerna]/pulic/lerna` to folder `[you]/public/lerna`

**10.** edit file `[you]/src/lerna/config.ts` and configure lerna

**ApiUrl :** GraphQL server url (without slash)
**UploadUrl :** upload server url (without slash)
**FileUploadedUrl :** base url for uploaded files (with slash)
**BaseUrl :** base url for lerna project (without slash)
**PanelContainer :** your project panel template

**11.** `[you]/src/lerna/LernaApp` file is entry of lerna app. use it inside `[you]/src/index.js` file for testing.

```react=
import LernaApp from './lerna/LernaApp';

ReactDOM.render(
	<LernaApp />,
	document.getElementById("root")
);
```

or combine LernaApp to your project

## Routes

root of your project = `[you]`
your `BaseUrl` in `config.ts` = `[base]`

route's constants are in `[you]/src/lerna/shared/links.ts`

**TeacherPageLink :** `[base]/teacher`
**TeacherTaskPageLink :** `[base]/teacher/task/{id}`
**TeacherTaskPageWithStudentLink :** `[base]/teacher/task/{id}/{studentId}`
**teacherTaskPageLinkSelect(id) :** `[base]/teacher/task/{id}`
**teacherTaskPageWithStudentLinkSelect(id, studentId) :** `[base]/teacher/task/{id}/{studentId}`

**StudentTaskPageLink :** `[base]/student/task/{id}`
**StudentPageLink :** `[base]/student`
**studentTaskPageLinkSelect(id) :** `[base]/student/task/{id}`

**MainPageLink :** `[base]`
**DialogsLink :** `[base]/dialogs`
