module.exports = {
  purge: ["./src/**/*.ts", "./src/**/*.tsx", "./src/**/*.js", "./src/**/*.jsx"],
  important: true,
  theme: {
    extend: {
      colors: {
        "natural-light": "#ECF1F2",
        "natural-medium": "#C3D1E0",
        "natural-dark": "#334D6E",
        success: "#4CAF50",
        warning: "#FF9800",
        error: "#F44336",
        grey: {
          50: "#FAFAFA",
          100: "#F5F5F5",
          200: "#EEEEEE",
          300: "#E0E0E0",
          400: "#BDBDBD",
          500: "#9E9E9E",
          600: "#757575",
          700: "#616161",
          800: "#424242",
          900: "#212121",
        },
      },
      spacing: {
        "2/3": "66.666667%",
        "9/16": "56.25%",
        "1/1": "100%",
        "3/4": "75%",
        28: "112px",
        31: "124px",
      },
      padding: {
        28: "7rem",
      },
      scale: {
        200: "2",
        300: "3",
      },
    },
    ripple: (theme) => ({
      colors: theme("colors"),
      darken: 0.1,
    }),
  },
  plugins: [require("tailwindcss-rtl"), require("tailwindcss-ripple")()],
};
